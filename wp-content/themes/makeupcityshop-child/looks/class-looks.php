<?php

/**
 * Class for Looks
 */
class Looks
{
	
	function __construct()
	{
		add_action( 'init', array($this, 'looks_register'), 0 );
		/* Metaboxes for looks */
		add_filter( 'rwmb_meta_boxes', array($this, 'looks_register_meta_boxes') );

		/* Modify Header Container for looks posttype */
		add_action( 'sober_before_content', array($this, 'looks_header_container'), 5 );

	}

	// Register Custom Post Type
	public function looks_register() {

		$labels = array(
			'name'                  => _x( 'Looks', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Look', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Looks', 'text_domain' ),
			'name_admin_bar'        => __( 'Look', 'text_domain' ),
			'archives'              => __( 'Look Archives', 'text_domain' ),
			'attributes'            => __( 'Look Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Look:', 'text_domain' ),
			'all_items'             => __( 'All Looks', 'text_domain' ),
			'add_new_item'          => __( 'Add New Look', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Look', 'text_domain' ),
			'edit_item'             => __( 'Edit Look', 'text_domain' ),
			'update_item'           => __( 'Update Look', 'text_domain' ),
			'view_item'             => __( 'View Look', 'text_domain' ),
			'view_items'            => __( 'View Looks', 'text_domain' ),
			'search_items'          => __( 'Search Look', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into Look', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Looks list', 'text_domain' ),
			'items_list_navigation' => __( 'Looks list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter Looks list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Look', 'text_domain' ),
			'description'           => __( 'Post Type Description', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'thumbnail' ),
			'taxonomies'            => array( 'looks_category' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'looks-temporary', $args );

		$taxonomy_labels = array(
			'name'                       => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Category', 'text_domain' ),
			'all_items'                  => __( 'All Categories', 'text_domain' ),
			'parent_item'                => __( 'Parent Category', 'text_domain' ),
			'parent_item_colon'          => __( 'Parent Category:', 'text_domain' ),
			'new_item_name'              => __( 'New Category Name', 'text_domain' ),
			'add_new_item'               => __( 'Add Category Item', 'text_domain' ),
			'edit_item'                  => __( 'Edit Category', 'text_domain' ),
			'update_item'                => __( 'Update Category', 'text_domain' ),
			'view_item'                  => __( 'View Category', 'text_domain' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'text_domain' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'text_domain' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
			'popular_items'              => __( 'Popular categories', 'text_domain' ),
			'search_items'               => __( 'Search categories', 'text_domain' ),
			'not_found'                  => __( 'Not Found', 'text_domain' ),
			'no_terms'                   => __( 'No categories', 'text_domain' ),
			'items_list'                 => __( 'Categories list', 'text_domain' ),
			'items_list_navigation'      => __( 'Categories list navigation', 'text_domain' ),
		);
		$taxonomy_args = array(
			'labels'                     => $taxonomy_labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'looks_category', array( 'looks-temporary' ), $taxonomy_args );

	}

	public function looks_register_meta_boxes( $meta_boxes ) {
	    $prefix = 'looks_';
	    $meta_boxes[] = array(
	        'id'         => 'information',
	        'title'      => 'Information',
	        'post_types' => 'looks-temporary',
	        'context'    => 'normal',
	        'priority'   => 'high',

	        'fields' => array(
	        	array(
	        	    'name'  => 'Product Bundle ID',
	        	    'desc'  => 'Enter product Bundle ID here',
	        	    'id'    => $prefix . 'bundle_id',
	        	    'type'  => 'number',
	        	),
	            array(
	                'name'  => 'Youtube Video ID',
	                'desc'  => 'Enter youtube video ID here',
	                'id'    => $prefix . 'youtube',
	                'type'  => 'text',
	            ),
	            array(
	                'name'  => 'Youtube Video Thumbnail',
	                'desc'  => 'Enter youtube video thumbnail here',
	                'id'    => $prefix . 'youtube_thumbnail',
	                'type'  => 'image_advanced'
	            ),
	            array(
	                'name'  => 'Banner Image',
	                'desc'  => 'Enter youtube banner image here',
	                'id'    => $prefix . 'banner_image',
	                'type'  => 'image_advanced'
	            ),
	            array(
	                'name'  => 'Package includes',
	                'desc'  => 'Enter package includes',
	                'id'    => $prefix . 'package_includes',
	                'type'  => 'text',
	                'clone' => true
	            ),
	            array(
	                'name'  => 'Detail',
	                'desc'  => 'Enter bundle detail here',
	                'id'    => $prefix . 'detail',
	                'type'  => 'wysiwyg',
	            )
	        )
	    );

	    // Add more meta boxes if you want
	    // $meta_boxes[] = ...

	    return $meta_boxes;
	}

	/**
	 * Open content container
	 */
	public function looks_header_container() {

		if ( is_post_type_archive( 'looks-temporary' ) ) {
			$class = 'sober-container';
		}

		$class = apply_filters( 'sober_site_content_container_class', $class );

		echo '<div class="' . $class . '">';
	}
}

new Looks();