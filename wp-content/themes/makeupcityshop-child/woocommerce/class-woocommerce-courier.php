<?php
/**
 * Handle Woocommerce_Couriers Class
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Woocommerce_Courier
{
	
	private $api_key;
	private $api_password;

	function __construct()
	{
		$this->api_key = 'F6DFF4245B6F76B5D3794C49032270AE';
		$this->api_password = 'B@YSINTERNATIONAL1';

		add_action('woocommerce_order_status_dispatched', array($this, 'dispatched_order_send_to_courier'), 200, 2);

		add_action( 'add_meta_boxes', array($this, 'leopard_cities') );
		add_action( 'add_meta_boxes', array($this, 'dispatch_date') );
	}

	/** 
	 * Send to courier when order status changed
	 * 
	**/
	public function dispatched_order_send_to_courier( $order_id, $order ) {
		
		$tn = get_post_meta($order_id, '_wc_shipment_tracking_items', true);
		if (!empty($tn)) {
			return;
		}

       $is_order_synced_with_technosys =  get_post_meta($order_id, '_is_synced_with_technosys', true);

       if(!empty($is_order_synced_with_technosys) && $is_order_synced_with_technosys !=="yes") {
	       $order->update_status('order-confirmed');
           $order->add_order_note( __( 'Error during status transition. order not synced with technosys', 'woocommerce' ));
           return;
       }


		$city = $this->get_order_city( $order->get_billing_city() );

		if ( !is_null($city) ) {
			$number_of_items = count($order->get_items());
			$total_amount = $order->get_total();
			$address = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2();
			$name = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
			$email = $order->get_billing_email();
			$phone = $order->get_billing_phone();
			$note = $order->get_customer_note();

			$place_order = $this->place_order_at_courier($number_of_items, $total_amount, $order_id, $city, $name, $email, $phone, $address, $note);

			$place_order_array = json_decode( $place_order, true );
			$place_order_status = $place_order_array['status'];

			if ( function_exists( 'wc_st_add_tracking_number' ) && $place_order_status == 1 ) {

				$tracking_number = $place_order_array['track_number'];
				$slip_link = $place_order_array['slip_link'];

				wc_st_add_tracking_number( $order_id, $tracking_number, 'Makeup City Shop', strtotime($order->get_date_modified()), $slip_link );
				$order->update_meta_data( 'dispatch_date', $order->get_date_modified() );
				$order->save();

			} else {
				$order->update_status('order-confirmed');
				$logger = wc_get_logger();
				$logger->error(
					sprintf( 'Status transition of order #%d errored!', $order_id ), array(
						'order' => $order,
						'error' => $place_order_array['error'],
					)
				);

				$order->add_order_note( __( 'Error during status transition. Recieved the following error from leopard: ', 'woocommerce' ) . ' ' . $place_order_array['error'] );
				
			}
		} else {
			$order->update_status('order-confirmed');
			$logger = wc_get_logger();
			$logger->error(
				sprintf( 'Status transition of order #%d errored!', $order_id ), array(
					'order' => $order,
					'error' => $place_order_array['error'],
				)
			);

			$order->add_order_note( __( 'Error during status transition. Order City not found in Leopards. Please update the city from Leopard Cities below and dispatch again. City: ', 'woocommerce' ) . ' ' . $city );

		}

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function get_order_city($city_required)
	{
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, 'http://new.leopardscod.com/webservice/getAllCities/format/json/'); // Write here Test or Production Link
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
		    'api_key' => $this->api_key,
		    'api_password' => $this->api_password
		));

		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		$city_array = json_decode( $buffer, true );

		$city_required = urlencode( strtolower( preg_replace('/\s+/', '_', $city_required) ) );

		foreach ($city_array['city_list'] as $city) {
			$cities[$city['id']] = urlencode( strtolower( preg_replace('/\s+/', '_', $city['name']) ) );
		}

		$city_key = array_search($city_required, $cities, TRUE);		

		return absint($city_key);
	}

	/**
	 * Place order at courier
	 *
	 * @return void
	 * @author 
	 **/
	public function place_order_at_courier($number_of_items, $total_amount, $order_id, $destination_city, $customer_name, $customer_email, $customer_phone, $customer_address, $customer_note)
	{
		$customer_note = empty($customer_note) ? 'None' : $customer_note;
		
		$order_array = array(
		    'api_key'                       => $this->api_key,
		    'api_password'                  => $this->api_password,
		    'booked_packet_weight'          => 200,            	// Weight should in 'Grams' e.g. '2000'
		    'booked_packet_no_piece'        => absint( $number_of_items ), // No. of Pieces should an Integer Value
		    'booked_packet_collect_amount'  => absint($total_amount), // Collection Amount on Delivery
		    'booked_packet_order_id'        => $order_id,            // Optional Filed, (If any) Order ID of Given Product
		    
		    'origin_city'                   => 'self',            /** Params: 'self' or 'integer_value' e.g. 'origin_city' => 'self' or 'origin_city' => 789 (where 789 is Lahore ID)
		                                                             * If 'self' is used then Your City ID will be used.
		                                                             * 'integer_value' provide integer value (for integer values read 'Get All Cities' api documentation)
		                                                             */
		    
		    'destination_city'              => $destination_city,            /** Params: 'self' or 'integer_value' e.g. 'destination_city' => 'self' or 'destination_city' => 789 (where 789 is Lahore ID)
		                                                             * If 'self' is used then Your City ID will be used.
		                                                             * 'integer_value' provide integer value (for integer values read 'Get All Cities' api documentation) 
		                                                             */
		    
		    'shipment_name_eng'             => 'self',            // Params: 'self' or 'Type any other Name here', If 'self' will used then Your Company's Name will be Used here
		    'shipment_email'                => 'self',            // Params: 'self' or 'Type any other Email here', If 'self' will used then Your Company's Email will be Used here
		    'shipment_phone'                => 'self',            // Params: 'self' or 'Type any other Phone Number here', If 'self' will used then Your Company's Phone Number will be Used here
		    'shipment_address'              => 'self',            // Params: 'self' or 'Type any other Address here', If 'self' will used then Your Company's Address will be Used here
		    'consignment_name_eng'          => $customer_name,            // Type Consignee Name here
		    'consignment_email'             => $customer_email,            // Optional Field (You can keep it empty), Type Consignee Email here
		    'consignment_phone'             => $customer_phone,            // Type Consignee Phone Number here
		    'consignment_address'           => $customer_address,            // Type Consignee Address here
		    'special_instructions'          => $customer_note,            // Type any instruction here regarding booked packet
		);

		$curl_handle = curl_init();

		// For Direct Link Access use below commented link
		//curl_setopt($curl_handle, CURLOPT_URL, 'http://new.leopardscod.com/webservice/trackBookedPacket/?api_key=XXXX&api_password=XXXX&track_numbers=XXXXXXXX');  // For Get Mother/Direct Link

		curl_setopt($curl_handle, CURLOPT_URL, 'http://new.leopardscod.com/webservice/bookPacket/format/json/');  // Write here Test or Production Link
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $order_array);

		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		return $buffer;
	}

	/**
	 * Add leopard city metaboxes
	 *
	 * @return void
	 * @author 
	 **/
	public function leopard_cities() {
	    add_meta_box(
	        'leopard_cities',
	        'Leopard Cities',
	        array($this, 'leopard_cities_callback'),
	        'shop_order',
	        'side',
	        'low'
	    );
	}

	/**
	 * Add leopard city metaboxes
	 *
	 * @return void
	 * @author 
	 **/
	public function dispatch_date() {
	    add_meta_box(
	        'dispatch_date',
	        'Order Details',
	        array($this, 'dispatch_date_callback'),
	        'shop_order',
	        'side',
	        'low'
	    );
	}

	/**
	 * Leopard city metabox callback
	 *
	 * @return void
	 * @author 
	 **/
	public function leopard_cities_callback( $post )
	{
	    $curl_handle = curl_init();
	    curl_setopt($curl_handle, CURLOPT_URL, 'http://new.leopardscod.com/webservice/getAllCities/format/json/'); // Write here Test or Production Link
	    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl_handle, CURLOPT_POST, 1);
	    curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
	        'api_key' => $this->api_key,
	        'api_password' => $this->api_password
	    ));

	    $buffer = curl_exec($curl_handle);
	    curl_close($curl_handle);

	    $city_array = json_decode( $buffer, true )['city_list'];

	    echo "<ul>";
	    foreach ($city_array as $city) {
	    	echo "<li>" . $city['name'] . "</li>";
	    }
	    echo "</ul>";
	}

	/**
	 * Leopard city metabox callback
	 *
	 * @return void
	 * @author 
	 **/
	public function dispatch_date_callback( $post )
	{
	    $dispatch_date = get_post_meta( $post->ID, 'dispatch_date', true );
		$sync_with_technosys = get_post_meta( $post->ID, '_is_synced_with_technosys', true );
		
	    if (!empty($dispatch_date)) {
	    	echo '<strong>Dispatch Date</strong> ' . date('d M Y', strtotime($dispatch_date)) . '</br>';
	    } else {
	    	echo "No dispatch date assigned</br>";
	    }
		
		if (!empty($sync_with_technosys) && ($sync_with_technosys === 'yes') ) {
	    	echo '<strong>Synced with Technosys</strong>: Yes ';
	    }	
	    
	}
	
}

new Woocommerce_Courier();
