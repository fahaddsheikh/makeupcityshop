<?php
/**
 * Handle Woocommerce_Customs Class
 */

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Woocommerce_Custom
{
    var $leopardCities;
    
    function __construct()
    {
        add_action('init', array($this, 'register_new_status'));

        add_filter('wc_order_statuses', array($this, 'add_new_status_to_order_statuses'));

        add_filter('wc_order_is_editable', array($this, 'wc_make_processing_orders_editable'), 10, 2);

        add_filter('woocommerce_reports_order_statuses', array($this, 'include_custom_order_status_to_reports'), 20, 1);

        add_filter('woocommerce_package_rates', array($this, 'hide_shipping_when_free_is_available'), 100);

        add_filter('wc_pip_document_table_headers', array($this, 'sv_wc_pip_invoice_add_brand_column'), 10, 3);
        add_filter('wc_pip_document_table_row_cells', array($this, 'sv_wc_pip_document_invoice_add_brand_cell'), 10, 5);

        add_filter("rest_shop_order_collection_params", array($this, 'modify_max_per_page'));
         
        add_filter('woocommerce_checkout_fields', array($this, 'remove_billing_postcode_checkout'));

        add_filter('woocommerce_checkout_fields', array($this, 'change_city_to_dropdown'));

        add_filter( 'woocommerce_admin_billing_fields' , array($this, 'admin_city_select_field') );
        add_filter( 'woocommerce_admin_shipping_fields' , array($this, 'admin_city_select_field') );

        $this->leopardCities = array(
            "Abbaspur (a.k)" => "Abbaspur (a.k)",
            "Abbottabad" => "Abbottabad",
            "Aboha" => "Aboha",
            "Adda 46 chak s" => "Adda 46 chak s",
            "Adda aujla kala" => "Adda aujla kala",
            "Adda pakhi more" => "Adda pakhi more",
            "Adda pensra" => "Adda pensra",
            "Adda phloor onl" => "Adda phloor onl",
            "Adda thikriwala" => "Adda thikriwala",
            "Adil pur" => "Adil pur",
            "Aghar jamalpur" => "Aghar jamalpur",
            "Agwana" => "Agwana",
            "Ahmad nagar" => "Ahmad nagar",
            "Ahmad pur east" => "Ahmad pur east",
            "Ahmad pur sial" => "Ahmad pur sial",
            "Akal garh (a.k)" => "Akal garh (a.k)",
            "Akri" => "Akri",
            "Ali pur syedan" => "Ali pur syedan",
            "Ali zai kurram" => "Ali zai kurram",
            "Alipur" => "Alipur",
            "Alipur chattha" => "Alipur chattha",
            "Alipur sayyadan" => "Alipur sayyadan",
            "Ambor area" => "Ambor area",
            "Ambriala chowk" => "Ambriala chowk",
            "Amin abad" => "Amin abad",
            "Amir pur mangan" => "Amir pur mangan",
            "Anoi srohta(a.k" => "Anoi srohta(a.k",
            "Arifabad" => "Arifabad",
            "Arifwala" => "Arifwala",
            "Arija" => "Arija",
            "Astore" => "Astore",
            "Attar shisha" => "Attar shisha",
            "Attock" => "Attock",
            "Attock khurd" => "Attock khurd",
            "Awan abad(bathi" => "Awan abad(bathi",
            "Awaran" => "Awaran",
            "Aziz abad (a.k." => "Aziz abad (a.k.",
            "Aziz chak" => "Aziz chak",
            "Babri banda" => "Babri banda",
            "Bacha band" => "Bacha band",
            "Badah" => "Badah",
            "Badin" => "Badin",
            "Badshahpur" => "Badshahpur",
            "Baggan" => "Baggan",
            "Bagh (a.k.)" => "Bagh (a.k.)",
            "Bagnoter" => "Bagnoter",
            "Bahawalnagar" => "Bahawalnagar",
            "Bahawalpur" => "Bahawalpur",
            "Bahuwal" => "Bahuwal",
            "Balakot" => "Balakot",
            "Balokhel city" => "Balokhel city",
            "Bangla yateem w" => "Bangla yateem w",
            "Bannu" => "Bannu",
            "Barnala (a.k)" => "Barnala (a.k)",
            "Basser pur" => "Basser pur",
            "Basti fuja" => "Basti fuja",
            "Batgram" => "Batgram",
            "Bathrui (a.k)" => "Bathrui (a.k)",
            "Batkhela" => "Batkhela",
            "Bazargai" => "Bazargai",
            "Beesham" => "Beesham",
            "Bela" => "Bela",
            "Bhagatanwala" => "Bhagatanwala",
            "Bhagwal" => "Bhagwal",
            "Bhai pheru" => "Bhai pheru",
            "Bhakkar" => "Bhakkar",
            "Bhall" => "Bhall",
            "Bhalwal" => "Bhalwal",
            "Bhara khu" => "Bhara khu",
            "Bhera" => "Bhera",
            "Bhikari kalan" => "Bhikari kalan",
            "Bhiki sharif" => "Bhiki sharif",
            "Bhiko more" => "Bhiko more",
            "Bhimber a. k" => "Bhimber a. k",
            "Bhit shah" => "Bhit shah",
            "Bhone" => "Bhone",
            "Bhopalwala" => "Bhopalwala",
            "Bhring (a.k)" => "Bhring (a.k)",
            "Bhubbar" => "Bhubbar",
            "Bhudial" => "Bhudial",
            "Bhurban pearl c" => "Bhurban pearl c",
            "Bidder marjan" => "Bidder marjan",
            "Bloouch (a.k.)" => "Bloouch (a.k.)",
            "Boken more" => "Boken more",
            "Brotha" => "Brotha",
            "Brund batha(a.k" => "Brund batha(a.k",
            "Bucheri" => "Bucheri",
            "Bunn behk (a.k)" => "Bunn behk (a.k)",
            "Bunner" => "Bunner",
            "Burewala" => "Burewala",
            "Burg attari" => "Burg attari",
            "Cadet coll. low" => "Cadet coll. low",
            "Cadet coll. sun" => "Cadet coll. sun",
            "Cadet college l" => "Cadet college l",
            "Cadet college(k" => "Cadet college(k",
            "Chak 168/10r" => "Chak 168/10r",
            "Chak abdullah" => "Chak abdullah",
            "Chak baqar shah" => "Chak baqar shah",
            "Chak bazaar(a.k" => "Chak bazaar(a.k",
            "Chak beli khan" => "Chak beli khan",
            "Chak jamal" => "Chak jamal",
            "Chak jhumra" => "Chak jhumra",
            "Chak kamal(sehn" => "Chak kamal(sehn",
            "Chak pirana" => "Chak pirana",
            "Chak swari(a.k)" => "Chak swari(a.k)",
            "Chakar kot koha" => "Chakar kot koha",
            "Chakdara" => "Chakdara",
            "Chaklala" => "Chaklala",
            "Chakothi (a.k.)" => "Chakothi (a.k.)",
            "Chakwal" => "Chakwal",
            "Chamankot (a.k)" => "Chamankot (a.k)",
            "Chambi" => "Chambi",
            "Changa manga" => "Changa manga",
            "Char bagh (swat" => "Char bagh (swat",
            "Charsadda" => "Charsadda",
            "Chashma colny." => "Chashma colny.",
            "Chashma(wapda-a" => "Chashma(wapda-a",
            "Chattar area" => "Chattar area",
            "Chechian (a.k.)" => "Chechian (a.k.)",
            "Chella bandi (a" => "Chella bandi (a",
            "Chenab nagar" => "Chenab nagar",
            "Chicha watni" => "Chicha watni",
            "Chiniot" => "Chiniot",
            "Chistian" => "Chistian",
            "Chitral" => "Chitral",
            "Chittar pari" => "Chittar pari",
            "Choa saidan sha" => "Choa saidan sha",
            "Chohar jamali" => "Chohar jamali",
            "Chomko" => "Chomko",
            "Chorai (a.k)" => "Chorai (a.k)",
            "Chottagla (a.k)" => "Chottagla (a.k)",
            "Chowk sarwar sh" => "Chowk sarwar sh",
            "Chowki (a.k)" => "Chowki (a.k)",
            "Chund" => "Chund",
            "Chung" => "Chung",
            "Dad laghari" => "Dad laghari",
            "Dadu" => "Dadu",
            "Dadyal (a.k)" => "Dadyal (a.k)",
            "Dahewal" => "Dahewal",
            "Daira din panna" => "Daira din panna",
            "Dalwal" => "Dalwal",
            "Dammas (a.k)" => "Dammas (a.k)",
            "Danda shah bila" => "Danda shah bila",
            "Dandli (a.k)" => "Dandli (a.k)",
            "Dar-ul-islam co" => "Dar-ul-islam co",
            "Darbar(hajiabad" => "Darbar(hajiabad",
            "Darbello" => "Darbello",
            "Darrora" => "Darrora",
            "Darrosh" => "Darrosh",
            "Daska" => "Daska",
            "Datote (a.k.)" => "Datote (a.k.)",
            "Daulat pur" => "Daulat pur",
            "Daulatpur safan" => "Daulatpur safan",
            "Daur" => "Daur",
            "Deena" => "Deena",
            "Depalpur" => "Depalpur",
            "Dera ala yar" => "Dera ala yar",
            "Dera ghazi khan" => "Dera ghazi khan",
            "Dera ismail khan" => "Dera ismail khan",
            "Dera malla sin" => "Dera malla sin",
            "Dera murad jama" => "Dera murad jama",
            "Dewan sharif (a" => "Dewan sharif (a",
            "Dhall ghair" => "Dhall ghair",
            "Dhander (kalan)" => "Dhander (kalan)",
            "Dhangri bala (a" => "Dhangri bala (a",
            "Dhani bombian" => "Dhani bombian",
            "Dharanwala" => "Dharanwala",
            "Dharki" => "Dharki",
            "Dheri julagram" => "Dheri julagram",
            "Dhoda" => "Dhoda",
            "Dhok daura (a.k" => "Dhok daura (a.k",
            "Dhoke badial" => "Dhoke badial",
            "Dhoke maken" => "Dhoke maken",
            "Dhurkana" => "Dhurkana",
            "Dhurnal" => "Dhurnal",
            "Digri" => "Digri",
            "Dinga" => "Dinga",
            "Dir" => "Dir",
            "Dokota" => "Dokota",
            "Dokri" => "Dokri",
            "Dolta" => "Dolta",
            "Domail" => "Domail",
            "Dongi (a.k)" => "Dongi (a.k)",
            "Dorey" => "Dorey",
            "Dorey wala" => "Dorey wala",
            "Dunya pur" => "Dunya pur",
            "Easakhel" => "Easakhel",
            "Eidgah road" => "Eidgah road",
            "Eiser (a.k)" => "Eiser (a.k)",
            "Elahabad" => "Elahabad",
            "Faisalabad" => "Faisalabad",
            "Faizpur khurd" => "Faizpur khurd",
            "Faqir wali" => "Faqir wali",
            "Faqirabad" => "Faqirabad",
            "Farooqa" => "Farooqa",
            "Farooqabad" => "Farooqabad",
            "Fateh jang" => "Fateh jang",
            "Fatehpur" => "Fatehpur",
            "Fatehpur (chak" => "Fatehpur (chak",
            "Fazilpur dhundh" => "Fazilpur dhundh",
            "Feroz watwan" => "Feroz watwan",
            "Ferozwala" => "Ferozwala",
            "Fort abbas" => "Fort abbas",
            "G maharaja" => "G maharaja",
            "G.s.sugar mills" => "G.s.sugar mills",
            "Gadoon amazai" => "Gadoon amazai",
            "Gaggo mandi" => "Gaggo mandi",
            "Gakkhar mandi" => "Gakkhar mandi",
            "Gala knatha" => "Gala knatha",
            "Gambat" => "Gambat",
            "Garhi dopatta" => "Garhi dopatta",
            "Garhi doulat za" => "Garhi doulat za",
            "Garhi habib ull" => "Garhi habib ull",
            "Garhi yasin" => "Garhi yasin",
            "Garih khairo" => "Garih khairo",
            "Gawadar" => "Gawadar",
            "Ghari khero" => "Ghari khero",
            "Gharibwal cemen" => "Gharibwal cemen",
            "Gharo" => "Gharo",
            "Ghazi" => "Ghazi",
            "Ghaziabad" => "Ghaziabad",
            "Ghazikot townsh" => "Ghazikot townsh",
            "Ghotki" => "Ghotki",
            "Ghulam ishaq un" => "Ghulam ishaq un",
            "Ghuman wala" => "Ghuman wala",
            "Gidani" => "Gidani",
            "Gilgit" => "Gilgit",
            "Glotian morr" => "Glotian morr",
            "Gogera" => "Gogera",
            "Goharabad" => "Goharabad",
            "Goi" => "Goi",
            "Gojra" => "Gojra",
            "Gojra ( mandi b" => "Gojra ( mandi b",
            "Golarchi" => "Golarchi",
            "Gomal universit" => "Gomal universit",
            "Gondal" => "Gondal",
            "Groat shehar/ca" => "Groat shehar/ca",
            "Guddu barrage" => "Guddu barrage",
            "Gujar khan" => "Gujar khan",
            "Gujarpur" => "Gujarpur",
            "Gujranwala" => "Gujranwala",
            "Gujranwala cant" => "Gujranwala cant",
            "Gujrat" => "Gujrat",
            "Gulabad" => "Gulabad",
            "Gulpur (a.k)" => "Gulpur (a.k)",
            "Gumbat" => "Gumbat",
            "Habibabad" => "Habibabad",
            "Habibullah colo" => "Habibullah colo",
            "Hafizabad" => "Hafizabad",
            "Haji abad" => "Haji abad",
            "Hajiabad (a.k)" => "Hajiabad (a.k)",
            "Hajiabad ichria" => "Hajiabad ichria",
            "Hajira (a.k.)" => "Hajira (a.k.)",
            "Hajiwala" => "Hajiwala",
            "Hala" => "Hala",
            "Halani" => "Halani",
            "Hamid" => "Hamid",
            "Hamid pur (a.k)" => "Hamid pur (a.k)",
            "Hangu" => "Hangu",
            "Harappa station" => "Harappa station",
            "Hari chand" => "Hari chand",
            "Hari ghel" => "Hari ghel",
            "Hariah railway" => "Hariah railway",
            "Haripur" => "Haripur",
            "Hariyawala" => "Hariyawala",
            "Harnai" => "Harnai",
            "Harnal" => "Harnal",
            "Harno" => "Harno",
            "Haroonabad" => "Haroonabad",
            "Haseeb waqas mi" => "Haseeb waqas mi",
            "Hasilpur" => "Hasilpur",
            "Hassan abdal" => "Hassan abdal",
            "Hati" => "Hati",
            "Hattar ind. est" => "Hattar ind. est",
            "Havelian" => "Havelian",
            "Haya serai" => "Haya serai",
            "Hazoor pur" => "Hazoor pur",
            "Hazro" => "Hazro",
            "Head baloki roa" => "Head baloki roa",
            "Head works(btk)" => "Head works(btk)",
            "Hera (chak 134" => "Hera (chak 134",
            "Hingorno" => "Hingorno",
            "Holar (a.k)" => "Holar (a.k)",
            "Hub chowki" => "Hub chowki",
            "Hujra shah muke" => "Hujra shah muke",
            "Hyder abad thal" => "Hyder abad thal",
            "Hyderabad" => "Hyderabad",
            "Ikhlas (dist. a" => "Ikhlas (dist. a",
            "Islam garh(a.k)" => "Islam garh(a.k)",
            "Islam pura jabb" => "Islam pura jabb",
            "Islamabad" => "Islamabad",
            "J camp" => "J camp",
            "Jacobabad" => "Jacobabad",
            "Jahania" => "Jahania",
            "Jaiserwala" => "Jaiserwala",
            "Jalabad (a.k.)" => "Jalabad (a.k.)",
            "Jalal pur bhatt" => "Jalal pur bhatt",
            "Jalal pur jattan" => "Jalal pur jattan",
            "Jalalpur pirwal" => "Jalalpur pirwal",
            "Jalalpur sharif" => "Jalalpur sharif",
            "Jaman shah(surs" => "Jaman shah(surs",
            "Jampur" => "Jampur",
            "Jamshoro" => "Jamshoro",
            "Jand" => "Jand",
            "Jand nagar" => "Jand nagar",
            "Jaranwala" => "Jaranwala",
            "Jarwar" => "Jarwar",
            "Jatlan (a.k.)" => "Jatlan (a.k.)",
            "Jauhrabad" => "Jauhrabad",
            "Jaunpur" => "Jaunpur",
            "Jehangira" => "Jehangira",
            "Jhajra east" => "Jhajra east",
            "Jhajra west" => "Jhajra west",
            "Jhang" => "Jhang",
            "Jharian" => "Jharian",
            "Jhelum" => "Jhelum",
            "Jheteke" => "Jheteke",
            "Jholanara (a.k)" => "Jholanara (a.k)",
            "Jhugian" => "Jhugian",
            "Jibbran mandi" => "Jibbran mandi",
            "Jinnahabad" => "Jinnahabad",
            "Jiwani" => "Jiwani",
            "Jor (bunner)" => "Jor (bunner)",
            "Juglot" => "Juglot",
            "Juna (a.k)" => "Juna (a.k)",
            "Jundathi (a.k)" => "Jundathi (a.k)",
            "Kabir wala" => "Kabir wala",
            "Kadhala (a.k)" => "Kadhala (a.k)",
            "Kahna nao" => "Kahna nao",
            "Kahror pakka" => "Kahror pakka",
            "Kahuta" => "Kahuta",
            "Kakool ( pma)" => "Kakool ( pma)",
            "Kala bagh" => "Kala bagh",
            "Kala bagh (p.a." => "Kala bagh (p.a.",
            "Kala shah kaku" => "Kala shah kaku",
            "Kalabat" => "Kalabat",
            "Kalah" => "Kalah",
            "Kalar kahar" => "Kalar kahar",
            "Kalary more(a.k" => "Kalary more(a.k",
            "Kalaswala" => "Kalaswala",
            "Kalhoro" => "Kalhoro",
            "Kallar saydian" => "Kallar saydian",
            "Kalor kot" => "Kalor kot",
            "Kamalia" => "Kamalia",
            "Kamber ali khan" => "Kamber ali khan",
            "Kamoki" => "Kamoki",
            "Kamra" => "Kamra",
            "Kamrotty (a.k)" => "Kamrotty (a.k)",
            "Kandal sayan" => "Kandal sayan",
            "Kandhkot" => "Kandhkot",
            "Kandiaro" => "Kandiaro",
            "Kanganpur" => "Kanganpur",
            "Kangra" => "Kangra",
            "Kanyal" => "Kanyal",
            "Kanyal (a.k)" => "Kanyal (a.k)",
            "Karachi" => "Karachi",
            "Karianwala" => "Karianwala",
            "Karimabad(hunza" => "Karimabad(hunza",
            "Kark" => "Kark",
            "Karkra town(a.k" => "Karkra town(a.k",
            "Karoondi" => "Karoondi",
            "Karor lal easan" => "Karor lal easan",
            "Kashmoor" => "Kashmoor",
            "Kasur" => "Kasur",
            "Kattha sughral" => "Kattha sughral",
            "Kerala majhan" => "Kerala majhan",
            "Keryala" => "Keryala",
            "Khaal" => "Khaal",
            "Khabeki" => "Khabeki",
            "Khad gujran(a.k" => "Khad gujran(a.k",
            "Khadro" => "Khadro",
            "Khaigala (a.k.)" => "Khaigala (a.k.)",
            "Khair pur meeru" => "Khair pur meeru",
            "Khairpur" => "Khairpur",
            "Khairpur nathan" => "Khairpur nathan",
            "Khairpur tamian" => "Khairpur tamian",
            "Khaiw wali" => "Khaiw wali",
            "Khalabat sector" => "Khalabat sector",
            "Khaliq abad(a.k" => "Khaliq abad(a.k",
            "Khan gah dogran" => "Khan gah dogran",
            "Khan ka sharif" => "Khan ka sharif",
            "Khan pur distt." => "Khan pur distt.",
            "Khanabad (a.k)" => "Khanabad (a.k)",
            "Khanewal" => "Khanewal",
            "Khanpur" => "Khanpur",
            "Khanpur meher" => "Khanpur meher",
            "Khanpur shomali" => "Khanpur shomali",
            "Khar (bajore ag" => "Khar (bajore ag",
            "Khar (batkhela)" => "Khar (batkhela)",
            "Kharian" => "Kharian",
            "Kharick" => "Kharick",
            "Khat kali" => "Khat kali",
            "Khawaja textile" => "Khawaja textile",
            "Khewra" => "Khewra",
            "Khidar wala" => "Khidar wala",
            "Khipro" => "Khipro",
            "Khorra" => "Khorra",
            "Khota" => "Khota",
            "Khuratta (a.k)" => "Khuratta (a.k)",
            "Khurrianwala" => "Khurrianwala",
            "Khushab" => "Khushab",
            "Khuzdar" => "Khuzdar",
            "Kohat" => "Kohat",
            "Kohat industria" => "Kohat industria",
            "Kohat(cement fa" => "Kohat(cement fa",
            "Kohlu" => "Kohlu",
            "Kohri (a.k.)" => "Kohri (a.k.)",
            "Kot abdul malik" => "Kot abdul malik",
            "Kot addu" => "Kot addu",
            "Kot chandnan" => "Kot chandnan",
            "Kot deji" => "Kot deji",
            "Kot digee" => "Kot digee",
            "Kot ghulam mohd" => "Kot ghulam mohd",
            "Kot islam" => "Kot islam",
            "Kot jamel (a.k)" => "Kot jamel (a.k)",
            "Kot je sing" => "Kot je sing",
            "Kot najeeb ulla" => "Kot najeeb ulla",
            "Kot radha kisha" => "Kot radha kisha",
            "Kot sujan sing" => "Kot sujan sing",
            "Kot sultan(bhai" => "Kot sultan(bhai",
            "Kota" => "Kota",
            "Kotla arab ali" => "Kotla arab ali",
            "Kotli (a. k)" => "Kotli (a. k)",
            "Kotri" => "Kotri",
            "Koulo tarrar" => "Koulo tarrar",
            "Kufri" => "Kufri",
            "Kunri" => "Kunri",
            "Kuthiala sheikh" => "Kuthiala sheikh",
            "Ladhana" => "Ladhana",
            "Lahore" => "Lahore",
            "Lahore hub" => "Lahore hub",
            "Laidher" => "Laidher",
            "Lakhi ghulam sh" => "Lakhi ghulam sh",
            "Lakki marwat" => "Lakki marwat",
            "Lala zar(chak 1" => "Lala zar(chak 1",
            "Lalamusa" => "Lalamusa",
            "Lallian" => "Lallian",
            "Lalpir (therma" => "Lalpir (therma",
            "Lalu rawank" => "Lalu rawank",
            "Larkana" => "Larkana",
            "Lasbela" => "Lasbela",
            "Lawrence colleg" => "Lawrence colleg",
            "Lawrencepur" => "Lawrencepur",
            "Layyah" => "Layyah",
            "Liaqatabad(a.k)" => "Liaqatabad(a.k)",
            "Liaqatabad(pipl" => "Liaqatabad(pipl",
            "Liaqatpur" => "Liaqatpur",
            "Lodhran" => "Lodhran",
            "Loothar" => "Loothar",
            "Luddan" => "Luddan",
            "Mach" => "Mach",
            "Machikey(factor" => "Machikey(factor",
            "Madina market" => "Madina market",
            "Maghal" => "Maghal",
            "Mailsi" => "Mailsi",
            "Maira matoor" => "Maira matoor",
            "Makadoompur pok" => "Makadoompur pok",
            "Makli" => "Makli",
            "Malakand" => "Malakand",
            "Malan mansoor" => "Malan mansoor",
            "Malhoona more" => "Malhoona more",
            "Malikwal" => "Malikwal",
            "Malowal" => "Malowal",
            "Manawala" => "Manawala",
            "Mandi bahauddin" => "Mandi bahauddin",
            "Mandi dhaba sin" => "Mandi dhaba sin",
            "Mandi heera sin" => "Mandi heera sin",
            "Mandi sadiq gun" => "Mandi sadiq gun",
            "Mandi safdar ab" => "Mandi safdar ab",
            "Mandi shah juin" => "Mandi shah juin",
            "Mandi usmanwala" => "Mandi usmanwala",
            "Mandra" => "Mandra",
            "Mandranwala" => "Mandranwala",
            "Manduri kurram" => "Manduri kurram",
            "Manga mandi" => "Manga mandi",
            "Mangla dam" => "Mangla dam",
            "Mangla hamlet (a.k.)" => "Mangla hamlet (a.k.)",
            "Mangla industri" => "Mangla industri",
            "Mansehra" => "Mansehra",
            "Manshera camp" => "Manshera camp",
            "Mardan" => "Mardan",
            "Mardwal" => "Mardwal",
            "Margaz" => "Margaz",
            "Mashkay" => "Mashkay",
            "Mastung" => "Mastung",
            "Matiyari" => "Matiyari",
            "Matli" => "Matli",
            "Matta (swat)" => "Matta (swat)",
            "Mehar" => "Mehar",
            "Mehmoodkot" => "Mehmoodkot",
            "Mehrabpur" => "Mehrabpur",
            "Mian channu" => "Mian channu",
            "Mian m.sugar mi" => "Mian m.sugar mi",
            "Mianwali" => "Mianwali",
            "Mingora (swat)" => "Mingora (swat)",
            "Mir pur baghal" => "Mir pur baghal",
            "Mir pur sakro" => "Mir pur sakro",
            "Mirokhan" => "Mirokhan",
            "Mirpur (a. k)" => "Mirpur (a. k)",
            "Mirpur khas" => "Mirpur khas",
            "Mirpur mathelo" => "Mirpur mathelo",
            "Mirupr bathoro" => "Mirupr bathoro",
            "Mithi" => "Mithi",
            "Mithiani" => "Mithiani",
            "Mitranwali" => "Mitranwali",
            "Mohar sharif" => "Mohar sharif",
            "Mohara gulsher" => "Mohara gulsher",
            "Mohinudin pur" => "Mohinudin pur",
            "Mohlanwal" => "Mohlanwal",
            "Mohmmadpur diwa" => "Mohmmadpur diwa",
            "Mohra noori" => "Mohra noori",
            "Mohuta mohra" => "Mohuta mohra",
            "Mong (a.k.)" => "Mong (a.k.)",
            "Morgah" => "Morgah",
            "Moro" => "Moro",
            "Mujahid abad (a" => "Mujahid abad (a",
            "Multan" => "Multan",
            "Murali wala" => "Murali wala",
            "Murid wala" => "Murid wala",
            "Muridke" => "Muridke",
            "Murree" => "Murree",
            "Musa" => "Musa",
            "Muzaffarabad(ak)" => "Muzaffarabad(ak)",
            "Muzaffargarh" => "Muzaffargarh",
            "Nakiyal (a.k)" => "Nakiyal (a.k)",
            "Nakkah bazar(a." => "Nakkah bazar(a.",
            "Nankana sahib" => "Nankana sahib",
            "Nar (a.k.)" => "Nar (a.k.)",
            "Narang mandi" => "Narang mandi",
            "Narian sharif" => "Narian sharif",
            "Narowal" => "Narowal",
            "Naseerabad" => "Naseerabad",
            "Naudero" => "Naudero",
            "Nawab shah" => "Nawab shah",
            "Nawan jandanwal" => "Nawan jandanwal",
            "Nehang" => "Nehang",
            "New afzalpur(ak" => "New afzalpur(ak",
            "New jatoi" => "New jatoi",
            "New mohalla" => "New mohalla",
            "Newdarband town" => "Newdarband town",
            "Nomanpura" => "Nomanpura",
            "Noor shah" => "Noor shah",
            "Nooriabad" => "Nooriabad",
            "Nooriabad" => "Nooriabad",
            "Noorpur noranga" => "Noorpur noranga",
            "Noorpur thal" => "Noorpur thal",
            "Nowshera" => "Nowshera",
            "Nowshera dt. kh" => "Nowshera dt. kh",
            "Nowshero feroz" => "Nowshero feroz",
            "Nrtc(telecom st" => "Nrtc(telecom st",
            "Okara" => "Okara",
            "Olympia chemica" => "Olympia chemica",
            "Oremara town" => "Oremara town",
            "Oughi" => "Oughi",
            "P.o.f. (factory" => "P.o.f. (factory",
            "Pahrianwali add" => "Pahrianwali add",
            "Paigah" => "Paigah",
            "Pakka khuh" => "Pakka khuh",
            "Pakpattan" => "Pakpattan",
            "Panag gali (a.k" => "Panag gali (a.k",
            "Pandi sabarwal" => "Pandi sabarwal",
            "Pandore" => "Pandore",
            "Pang kasi" => "Pang kasi",
            "Panjeera (a.k.)" => "Panjeera (a.k.)",
            "Pano aqil" => "Pano aqil",
            "Panyam (a.k)" => "Panyam (a.k)",
            "Panyiola (a.k.)" => "Panyiola (a.k.)",
            "Par nowshera" => "Par nowshera",
            "Parachinar" => "Parachinar",
            "Parova" => "Parova",
            "Pasrur" => "Pasrur",
            "Pattika (a.k.)" => "Pattika (a.k.)",
            "Pattoki" => "Pattoki",
            "Pcfl -hpfl" => "Pcfl -hpfl",
            "Perial" => "Perial",
            "Peshawar" => "Peshawar",
            "Phaki shah mard" => "Phaki shah mard",
            "Phullarwan" => "Phullarwan",
            "Pind dadan khan" => "Pind dadan khan",
            "Pind hashim kha" => "Pind hashim kha",
            "Pind kalan(a.k)" => "Pind kalan(a.k)",
            "Pind khurd(a.k)" => "Pind khurd(a.k)",
            "Pindi bhattian" => "Pindi bhattian",
            "Pindi gheb" => "Pindi gheb",
            "Pioneer cement" => "Pioneer cement",
            "Pir jagi(chak 1" => "Pir jagi(chak 1",
            "Pir jo goth" => "Pir jo goth",
            "Pirmahal" => "Pirmahal",
            "Pithoro" => "Pithoro",
            "Plak (a.k)" => "Plak (a.k)",
            "Potha (a.k.)" => "Potha (a.k.)",
            "Pourmiana" => "Pourmiana",
            "Public school(a" => "Public school(a",
            "Pulendri (a.k)" => "Pulendri (a.k)",
            "Pull 111 chak" => "Pull 111 chak",
            "Pull bager" => "Pull bager",
            "Pull manda (a.k" => "Pull manda (a.k",
            "Pull no. 12 mel" => "Pull no. 12 mel",
            "Punjab sugar mi" => "Punjab sugar mi",
            "Purab klair" => "Purab klair",
            "Qaboola sharif" => "Qaboola sharif",
            "Qadirabad" => "Qadirabad",
            "Qadirpur rawan" => "Qadirpur rawan",
            "Qalandarabad" => "Qalandarabad",
            "Qasba grt. chak" => "Qasba grt. chak",
            "Qazi ahmad" => "Qazi ahmad",
            "Qila dedar sing" => "Qila dedar sing",
            "Qillan (a.k.)" => "Qillan (a.k.)",
            "Quetta" => "Quetta",
            "Rabwa" => "Rabwa",
            "Radhen" => "Radhen",
            "Rahim yar khan" => "Rahim yar khan",
            "Raiwind" => "Raiwind",
            "Raja sadokey" => "Raja sadokey",
            "Rajanpur" => "Rajanpur",
            "Rakhni" => "Rakhni",
            "Rangla (a.k.)" => "Rangla (a.k.)",
            "Rani pur" => "Rani pur",
            "Ranjhi" => "Ranjhi",
            "Rasool pur tarr" => "Rasool pur tarr",
            "Ratodero" => "Ratodero",
            "Rawalakot (a.k)" => "Rawalakot (a.k)",
            "Rawalpindi" => "Rawalpindi",
            "Rawat" => "Rawat",
            "Rehmani nagar" => "Rehmani nagar",
            "Renalakhurd" => "Renalakhurd",
            "Right bank colo" => "Right bank colo",
            "Risal pur" => "Risal pur",
            "Rohri" => "Rohri",
            "Roodo sultan" => "Roodo sultan",
            "Saboor sharif" => "Saboor sharif",
            "Sacote" => "Sacote",
            "Sadiqabad" => "Sadiqabad",
            "Safdarabad" => "Safdarabad",
            "Sahib abad" => "Sahib abad",
            "Sahiwal" => "Sahiwal",
            "Sahiwal (nawan" => "Sahiwal (nawan",
            "Saidu sharif (s" => "Saidu sharif (s",
            "Sakrand" => "Sakrand",
            "Saloi" => "Saloi",
            "Sambrial" => "Sambrial",
            "Samror (a.k.)" => "Samror (a.k.)",
            "Samundri" => "Samundri",
            "Sandhianwali" => "Sandhianwali",
            "Sandu gali" => "Sandu gali",
            "Sanghar" => "Sanghar",
            "Sanghi" => "Sanghi",
            "Sangla hill" => "Sangla hill",
            "Sara-e- karishan" => "Sara-e- karishan",
            "Sarai alamgir" => "Sarai alamgir",
            "Sarai chowk" => "Sarai chowk",
            "Sarai naurang" => "Sarai naurang",
            "Sarai naymat kh" => "Sarai naymat kh",
            "Saranda (a.k)" => "Saranda (a.k)",
            "Sardarpur" => "Sardarpur",
            "Sargodha" => "Sargodha",
            "Sarkalan" => "Sarkalan",
            "Saro zai" => "Saro zai",
            "Saroki" => "Saroki",
            "Sarran (a.k.)" => "Sarran (a.k.)",
            "Sarsawa (a.k.)" => "Sarsawa (a.k.)",
            "Sarwala" => "Sarwala",
            "Satiyana" => "Satiyana",
            "Sattrah" => "Sattrah",
            "Sehar mandi(a.k" => "Sehar mandi(a.k",
            "Seher bagla" => "Seher bagla",
            "Sehnsa (a.k.)" => "Sehnsa (a.k.)",
            "Sehwan" => "Sehwan",
            "Shadadpur" => "Shadadpur",
            "Shah bagh" => "Shah bagh",
            "Shah dher" => "Shah dher",
            "Shahdadkot" => "Shahdadkot",
            "Shahdara" => "Shahdara",
            "Shahkot" => "Shahkot",
            "Shahpur city" => "Shahpur city",
            "Shakar garh" => "Shakar garh",
            "Shamkot" => "Shamkot",
            "Shankiari" => "Shankiari",
            "Sharaqpur shari" => "Sharaqpur shari",
            "Sheikhupura" => "Sheikhupura",
            "Sher khanie" => "Sher khanie",
            "Sher shah" => "Sher shah",
            "Shewa adda" => "Shewa adda",
            "Shikarpur" => "Shikarpur",
            "Shimla hill" => "Shimla hill",
            "Shorkot cantt." => "Shorkot cantt.",
            "Shorkot city" => "Shorkot city",
            "Shoukat lines" => "Shoukat lines",
            "Shujabad" => "Shujabad",
            "Sial sharif" => "Sial sharif",
            "Sialkot" => "Sialkot",
            "Sibbi" => "Sibbi",
            "Singola (a.k.)" => "Singola (a.k.)",
            "Sirgudhan" => "Sirgudhan",
            "Sita road(rehma" => "Sita road(rehma",
            "Smahni (a.k)" => "Smahni (a.k)",
            "Small ind estat" => "Small ind estat",
            "Sohawa" => "Sohawa",
            "Sohawa (city on" => "Sohawa (city on",
            "Sohawa dewalian" => "Sohawa dewalian",
            "Srohta (a.k.)" => "Srohta (a.k.)",
            "Sui" => "Sui",
            "Sujawal" => "Sujawal",
            "Sukkur" => "Sukkur",
            "Supply (a.k.)" => "Supply (a.k.)",
            "Swabi" => "Swabi",
            "Syed wala" => "Syed wala",
            "T-tip colony" => "T-tip colony",
            "Takht-e-bhai" => "Takht-e-bhai",
            "Takya kawan(a.k" => "Takya kawan(a.k",
            "Talagang" => "Talagang",
            "Talash ( ziarat" => "Talash ( ziarat",
            "Talian (a.k.)" => "Talian (a.k.)",
            "Tamirgaraha" => "Tamirgaraha",
            "Tand koi" => "Tand koi",
            "Tandlianwala" => "Tandlianwala",
            "Tando adam" => "Tando adam",
            "Tando ala yar" => "Tando ala yar",
            "Tando bago" => "Tando bago",
            "Tando ghulam al" => "Tando ghulam al",
            "Tando jam" => "Tando jam",
            "Tando jan mohd." => "Tando jan mohd.",
            "Tando mohd.khan" => "Tando mohd.khan",
            "Tangdew (a.k)" => "Tangdew (a.k)",
            "Tangi gala (a." => "Tangi gala (a.",
            "Tank" => "Tank",
            "Tappi" => "Tappi",
            "Tarbela dam" => "Tarbela dam",
            "Tariq abad (a.k" => "Tariq abad (a.k",
            "Tarnab farm(agr" => "Tarnab farm(agr",
            "Tarnol" => "Tarnol",
            "Tata pani (a.k)" => "Tata pani (a.k)",
            "Tatral" => "Tatral",
            "Taunsa sharif" => "Taunsa sharif",
            "Taxila" => "Taxila",
            "Tehi" => "Tehi",
            "Thaingi" => "Thaingi",
            "Thall" => "Thall",
            "Tharparker" => "Tharparker",
            "Thatha" => "Thatha",
            "Thatta (sadiqab" => "Thatta (sadiqab",
            "Theeng more(all" => "Theeng more(all",
            "Thehri" => "Thehri",
            "Theri mir wah" => "Theri mir wah",
            "Thoa maharam kh" => "Thoa maharam kh",
            "Thorar (a.k.)" => "Thorar (a.k.)",
            "Thotha rai baha" => "Thotha rai baha",
            "Thull" => "Thull",
            "Tibba sultan pu" => "Tibba sultan pu",
            "Toba tek sing" => "Toba tek sing",
            "Topa (a.k.)" => "Topa (a.k.)",
            "Topi" => "Topi",
            "Tranda mohd. pa" => "Tranda mohd. pa",
            "Trarkhail (a.k)" => "Trarkhail (a.k)",
            "Trutta (a.k)" => "Trutta (a.k)",
            "Tulamba" => "Tulamba",
            "Turbat" => "Turbat",
            "Uch sharif" => "Uch sharif",
            "Umer kot" => "Umer kot",
            "Usta muhammad" => "Usta muhammad",
            "Uthal" => "Uthal",
            "Vanky tarer" => "Vanky tarer",
            "Vegowal" => "Vegowal",
            "Vehalizer" => "Vehalizer",
            "Vehari" => "Vehari",
            "Venktarar" => "Venktarar",
            "Vespa factory" => "Vespa factory",
            "Wadhi" => "Wadhi",
            "Wah cantt" => "Wah cantt",
            "Wahhndo" => "Wahhndo",
            "Wanjari" => "Wanjari",
            "Warah" => "Warah",
            "Wasu" => "Wasu",
            "Wazirabad" => "Wazirabad",
            "Winder" => "Winder",
            "Yazman mandi" => "Yazman mandi",
            "Yousaf sugar mi" => "Yousaf sugar mi",
            "Zafarwal" => "Zafarwal"
        );

    }
     
    /**
     * Modify API Perpage
     *
    **/
    public function modify_max_per_page($params)
    {
        $params['per_page']['maximum'] = 10000;
        return $params;
    }


    /**
     * Register new  Order status
     *
    **/
    public function register_new_status()
    {
        register_post_status('wc-dispatched', array(
            'label'                     => 'Dispatched',
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop('Dispatched <span class="count">(%s)</span>', 'Dispatched <span class="count">(%s)</span>')
        ));
        register_post_status('wc-order-confirmed', array(
            'label'                     => 'Order Confirmed',
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop('Order Confirmed <span class="count">(%s)</span>', 'Order Confirmed <span class="count">(%s)</span>')
        ));
    }

    /**
     * Add to list of WC Order statuses
     *
    **/
    public function add_new_status_to_order_statuses($order_statuses)
    {
        $new_order_statuses = array();
        // add new order status after processing
        foreach ($order_statuses as $key => $status) {
            $new_order_statuses[ $key ] = $status;
            if ('wc-processing' === $key) {
                $new_order_statuses['wc-dispatched'] = 'Dispatched';
            }
            if ('wc-completed' === $key) {
                $new_order_statuses['wc-order-confirmed'] = 'Order Confirmed';
            }
        }
        return $new_order_statuses;
    }


    /**
     * Make Processing Orders Editable
     *
    **/
    public function wc_make_processing_orders_editable($is_editable, $order)
    {
        if ($order->get_status() == 'processing' || $order->get_status() == 'dispatched' ) {
            $is_editable = true;
        }

        return $is_editable;
    }

    /**
     * Include Custom Order Status in Reports
     *
    **/
    public function include_custom_order_status_to_reports($statuses)
    {
        // Adding the custom order status to the 3 default woocommerce order statuses
        return array( 'processing', 'in-progress', 'completed', 'on-hold', 'dispatched', 'order-confirmed' );
    }

    /**
     * Hide shipping rates when free shipping is available.
     * Updated to support WooCommerce 2.6 Shipping Zones.
     *
     * @param array $rates Array of rates found for the package.
     * @return array
     */
    public function hide_shipping_when_free_is_available($rates)
    {
        $free = array();
        foreach ($rates as $rate_id => $rate) {
            if ('free_shipping' === $rate->method_id) {
                $free[ $rate_id ] = $rate;
                break;
            }
        }
        return ! empty($free) ? $free : $rates;
    }
    
     // only copy if needed
    /**
     * Filter the document table headers to add product unit price.
     *
     * @param string[] $headers table column headers
     * @param int $order_id the order ID for the document
     * @param string $document_type the current document type
     * @return string[] updated table headers
     */
    function sv_wc_pip_invoice_add_brand_column($headers, $order_id, $document_type)
    {
        if ('invoice' === $document_type) {
            $new_headers = array();
            foreach ($headers as $key => $header) {
                if ('product' === $key) {
                    $new_headers['brand'] = __('Brand', 'textdomain');
                }
                if ('product' === $key) {
                    $new_headers['range'] = __('Range', 'textdomain');
                }
                $new_headers[ $key ] = $header;
            }
            $headers = $new_headers;
        }
        return $headers;
    }

    /**
     * Filter the document table row cells to add product unit price.
     *
     * @param string[] $cells the current table row cells
     * @param string $type WC_PIP_Document type
     * @param int $item_id item id
     * @param string[] $item item data
     * @param \WC_Product $product product object
     * @return string[] updated row cells
     */
    public function sv_wc_pip_document_invoice_add_brand_cell($cells, $document_type, $item_id, $item, $product)
    {
        if ('invoice' === $document_type) {
            $new_cells = array();
            foreach ($cells as $key => $cell) {
                if ('product' === $key) {
                    $new_cells['brand'] = $product->get_attribute('pa_brand');
                }
                if ('product' === $key) {
                    $new_cells['range'] = $product->get_attribute('pa_range');
                }
                $new_cells[ $key ] = $cell;
            }
            $cells = $new_cells;
        }
        return $cells;
    }

    /**
    * @snippet Remove the Postcode Field
    * on the WooCommerce Checkout
    *
    * @author Fahad Sheikh
    */
    public function remove_billing_postcode_checkout($fields)
    {
        unset($fields['billing']['billing_postcode']);
        return $fields;
    }

    /**
     * Change the checkout city field to a dropdown field.
     */
    public function change_city_to_dropdown($fields)
    {
        $city_args = wp_parse_args(array(
            'type' => 'select',
            'options' => $this->leopardCities
        ), $fields['shipping']['shipping_city']);
        $fields['shipping']['shipping_city'] = $city_args;
        $fields['billing']['billing_city'] = $city_args; // Also change for billing field
        return $fields;
    }

    function admin_city_select_field( $fields ) {

        $fields['city'] = array(
            'label'   => __( 'City', 'woocommerce' ),
            'show'    => false,
            'class'   => 'js_field-city select short',
            'type'    => 'select',
            'options' => $this->leopardCities
        );

        return $fields;
    }

}

new Woocommerce_Custom();
