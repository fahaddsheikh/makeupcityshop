<?php get_header( ); ?>
<?php 

/* Get Meta */
$bundle_id 			= rwmb_meta( 'looks_bundle_id' );
$thumbnail 			= rwmb_meta( 'looks_youtube_thumbnail', array( 'size' => 'medium_large', 'limit' => 1 ) );
$thumbnail_url 		= esc_url_raw( $thumbnail[0]['url'] );
$video_id 			= rwmb_meta( 'looks_youtube' );
$package_includes 	= rwmb_meta( 'looks_package_includes' );
$bundle_details 	= rwmb_meta('looks_detail');

$product = wc_get_product( $bundle_id );
$bundled_items = WC_PB_DB::query_bundled_items( array(
    'return'    => 'id=>product_id',
    'bundle_id' => array( absint( $bundle_id ) )
) );
$terms = get_the_terms( get_the_ID(), 'looks_category' );
?>

<div class="single-looks">
	<section class="video">
		<div id="voverlay" class="overlay" style="background-image:url(<?php echo $thumbnail_url; ?>);"></div>
		<div id="ytplayer"></div>
	</section>
	<section class="details">
		<div class="col-md-6 includes">
			<h1><span>This</span> Package Includes</h1>
			<div class="row">
				<?php foreach ($package_includes as $package_include) { ?>
					<li class="col-md-6"><?php echo $package_include; ?></li>
				<?php } ?>
			</div>
		</div>
		<div class="col-md-6">
			<div class="price-box">
				<span class="category">
					<?php 
					foreach ( $terms as $term ) {
					        echo $term->name;
					    }
					?>
				</span>
				<h1><?php echo the_title(); ?></h1>
				<h3><?php echo $product->get_price_html(); ?></h3>
				<a class="btn" href="<?php echo get_the_permalink( ); ?>?add-to-cart=<?php echo $bundle_id ?>"><svg viewBox="0 0 20 20"><use xlink:href="#basket-addtocart"></use></svg>Add to cart</a>
			</div>
		</div>
	</section>
	<section class="text-content">
		<h4>Details</h4>
		<?php echo wp_kses_post( $bundle_details ); ?>
	</section>

	<section class="woocommerce bundled-items">
		<h3>Individual Products</h3>
		<ul class="products columns-5">
			<?php foreach ($bundled_items as $bundled_item) {
				$post = get_post( $bundled_item, OBJECT );
				setup_postdata( $post );
				do_action( 'woocommerce_shop_loop' );
				wc_get_template_part( 'content', 'product' );
			} ?>
		</ul>
	</section>
</div>
<script>
 	// Load the IFrame Player API code asynchronously.
  	var tag = document.createElement('script');
  	tag.src = "https://www.youtube.com/player_api";
  	var firstScriptTag = document.getElementsByTagName('script')[0];
  	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  	// Replace the 'ytplayer' element with an <iframe> and
  	// YouTube player after the API code downloads.
  	// var player;
  	jQuery('#ytplayer').hide();
	var player;
	function onYouTubeIframeAPIReady() {
	 player = new YT.Player('ytplayer', {
	     videoId: '<?php echo $video_id; ?>',	     
	     height: '360',
	     width: '640',
	     playerVars: {
	         mute: 0,
	         autoplay: 0,
	         loop: 0,
	         controls: 1,
	         showinfo: 0,
	         autohide: 0,
	         vq: 'hd1080'
	     },
	     allowfullscreen: 1,
	     events: {
	         'onStateChange': onPlayerStateChange,
	         'onPlayerReady': onPlayerReady
	     }
	 });
	}
	function onPlayerStateChange(el) {		
		jQuery('#voverlay').show();
		jQuery('#ytplayer').hide();
	}

	function onPlayerReady(event) { // as youtube player will ready
	    jQuery('#voverlay').click(function() {  // it will wait to click on overlay/image
	        event.target.playVideo();  // as overlay image clicked video plays.
	    });
	}

	jQuery(document).ready(function($) {
	    $('#ytplayer').hide(); // on document ready youtube player will be hiden.
	    $('#voverlay').click(function() {  // as user click on overlay image.
	        $('#ytplayer').show();    // player will be visible to user 
	        $('#voverlay').hide(); // and overlay image will be hidden.
	    });
	});
</script>

<?php get_footer( ); ?>