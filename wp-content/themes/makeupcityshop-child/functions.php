<?php
/**
 * Sober functions and definitions.
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Sober Child
 */

/**
 * Utilities.
 */
require_once(__DIR__  . '/woocommerce/class-woocommerce-custom.php');
require_once(__DIR__  . '/woocommerce/class-woocommerce-courier.php');

add_filter( 'woocommerce_product_backorders_allowed', '__return_false', 1000 );

add_action('wp_head', 'head_facebook_verification');

function head_facebook_verification() { ?>

  <meta name="facebook-domain-verification" content="grw2m97ugabplikwvoo2obxy7cqncn" />

<?php };


add_filter('wpsl_templates', 'custom_templates');

function custom_templates($templates)
{

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-template.php',
    );

    return $templates;
}

//add_action('sober_after_site', 'facebook_messenger_api', 20);

function facebook_messenger_api()
{
    ?>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="292297744118139"
      theme_color="#E71D36"
      logged_in_greeting="Hi! Welcome to Coconut Island. How may we serve you?"
      logged_out_greeting="Hi! Welcome to Coconut Island. How may we serve you?">
    </div>
<?php }

add_action('sober_after_site', 'crazy_egg', 20);

function crazy_egg()
{
    ?>
    <!-- Hotjar Tracking Code for https://makeupcityshop.com/ -->
  <script>

      (function(h,o,t,j,a,r){

          h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};

          h._hjSettings={hjid:1633899,hjsv:6};

          a=o.getElementsByTagName('head')[0];

          r=o.createElement('script');r.async=1;

          r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;

          a.appendChild(r);

      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

  </script>


<?php }




add_action('wp_enqueue_scripts', 'sober_child_enqueue_scripts', 100);
add_filter('bulk_actions-edit-shop_order', 'bulk_order_status_actions', 50, 1);
function bulk_order_status_actions($actions)
{
    $new_actions = array();
    // add new order status before processing
    foreach ($actions as $key => $action) {
        if ('mark_completed' === $key) {
            $new_actions['mark_dispatched'] = __('Mark Dispatched', 'woocommerce');
        }
            $new_actions[$key] = $action;
    }
    return $new_actions;
}

function sober_child_enqueue_scripts()
{
    wp_enqueue_style('sober-child', get_stylesheet_uri(), array(), '2.0');
    wp_enqueue_script('custom-js-ssa', get_stylesheet_directory_uri()  . '/assets/custom.js', array('jquery', 'selectWoo'), '1.1');
}


add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_for_menus', 1, 2);

function wc_reg_for_menus($register, $name = '')
{
    if ($name == 'pa_brand') {
        $register = true;
    }
     return $register;
}

/* Plugin Name: First name plus last name as default display name. */
add_action('user_register', 'wpse_67444_first_last_display_name');

function wpse_67444_first_last_display_name($user_id)
{
    $data = get_userdata($user_id);
    // check if these data are available in your real code!
    wp_update_user(
        array (
            'ID' => $user_id,
            'display_name' => "$data->first_name $data->last_name"
        )
    );
}



/**
 * @snippet       WooCommerce Holiday/Pause Mode
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=20862
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.5.1
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
// Trigger Holiday Mode
 
//add_action('init', 'bbloomer_woocommerce_holiday_mode');
 
 
// Disable Cart, Checkout, Add Cart
 
function bbloomer_woocommerce_holiday_mode()
{
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
    remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20);
    remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);
    add_action('woocommerce_before_main_content', 'bbloomer_wc_shop_disabled', 5);
    add_action('woocommerce_before_cart', 'bbloomer_wc_shop_disabled', 5);
    add_action('woocommerce_before_checkout_form', 'bbloomer_wc_shop_disabled', 5);
}
 
 
// Show Holiday Notice
 
function bbloomer_wc_shop_disabled()
{
    wc_print_notice('The site is under maintainence mode. Please try again in a few moments.', 'error');
}

//add_action( 'init', 'recalc' );
function recalc() {
  global $wpdb;
  $orders = array( 77643 );

  foreach ($orders as $order_id) {
    $order          = wc_get_order($order_id);
    if(!$order) {
        continue;
    }
    $total_tax      = $order->get_total_tax();
    $items          = $order->get_items();
    $coupon_codes   = '';
    $order_subtotal = 0;
    $product_ids    = array();
    if (!empty($total_tax)) {
      foreach ($order->get_items('tax') as $item) {
        $rate_percent =  $item->get_rate_percent();
      }
    }
    if ($order->get_coupon_codes()) {
      foreach ($order->get_coupon_codes() as $coupon) {
        $coupon_code        =   $coupon;
        $coupon_post_obj    =   get_page_by_title($coupon_code, OBJECT, 'shop_coupon');
        $coupon_id          =   $coupon_post_obj->ID;
        $product_ids        =   get_post_meta($coupon_id, 'product_ids', true);
      }
      $product_ids = explode(',', $product_ids);
    }
    $t_regular_price = 0;
    $t_sale_price = 0;
    foreach ($items as $item) {
      $order_item_id          = $item->get_id();
      $product_id             = $item->get_product_id();
      $product                = wc_get_product($product_id);
      $item_data = $item->get_data();
      
      if ($product->is_on_sale()) {
        $order_item_id   = $item->get_id();
        $quantity        = $item->get_quantity();
        $regular_price   = $product->get_regular_price();
        $sale_price      = $product->get_sale_price();
        $t_regular_price = $regular_price * $quantity;
        $t_sale_price    = $sale_price * $quantity;
        wc_update_order_item_meta($order_item_id, '_line_subtotal', $t_regular_price);
        wc_update_order_item_meta($order_item_id, '_line_total', $t_sale_price);
        wc_update_order_item_meta($order_item_id, 'Sale item', 'Yes');
        wc_update_order_item_meta($order_item_id, '_regular_price', $regular_price);
        wc_update_order_item_meta($order_item_id, '_sale_price', $sale_price);
      } else {
        $line_subtotal      =   wc_get_order_item_meta($order_item_id, '_line_subtotal', true);
        $line_total         =   wc_get_order_item_meta($order_item_id, '_line_total', true);
        wc_update_order_item_meta($order_item_id, '_regular_price', $line_subtotal);
        if (in_array($product_id, $product_ids)) {
          wc_update_order_item_meta($order_item_id, 'Coupon item', 'Yes');
        } else {
          if ($line_subtotal>$line_total) {
            wc_update_order_item_meta($order_item_id, 'Coupon item', 'Yes');
          }
        }
      }
      $line_subtotal      =   wc_get_order_item_meta($order_item_id, '_line_subtotal', true);
      $order_subtotal     =   $order_subtotal + $line_subtotal;
      $line_total         =   wc_get_order_item_meta($order_item_id, '_line_total', true);
      $discount_item      =   $line_subtotal - $line_total;
      $item_tax           =   ($line_subtotal * $rate_percent) / 100;
      
      // update item GST taxs
      $item_taxes_array = $item_data['taxes'];
      $item_taxes_array['total']  =  array(1=>$item_tax);
      $item_taxes_array['subtotal']  = array(1=>$item_tax);
      wc_update_order_item_meta($order_item_id, '_line_tax_data', $item_taxes_array);
      wc_update_order_item_meta($order_item_id, '_line_subtotal_tax', $item_tax);
      wc_update_order_item_meta($order_item_id, '_line_tax', $item_tax);
      wc_update_order_item_meta($order_item_id, '_discount_item', $discount_item);
    }
    $order->update_meta_data('order_subtotal', $order_subtotal);
  }
}

add_action('woocommerce_thankyou', 'sync_status_meta_flag');

function sync_status_meta_flag($order_id)
{
    update_post_meta($order_id, '_is_synced_with_technosys', "no");
}