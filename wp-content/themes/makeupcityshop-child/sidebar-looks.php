<?php 
$terms = get_terms( array(
    'taxonomy' => 'looks_category',
    'hide_empty' => false,
) );
?>
<?php 

if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
	<aside id="secondary" class="widget-area primary-sidebar col-md-4 shop-sidebar " role="complementary">
		<section id="woocommerce_product_categories-1" class="widget woocommerce widget_product_categories">
			<h2 class="widget-title">Categories</h2>
			<?php 
    			echo '<ul class="product-categories">';
				    foreach ( $terms as $term ) {
				        echo '<li class="cat-item cat-item-48 cat-parent">';
				        echo '<a href="'.get_term_link($term->term_id).'">';
				        echo $term->name;
				        echo '</a>';
				        echo '</li>';
				    }
				echo '</ul>'; ?>
		</section>
	</aside>
<?php } ?>