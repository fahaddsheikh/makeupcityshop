<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Adds Brand_Widget widget.
 */
class Brand_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {

		global $wpdb;

		parent::__construct(
			'brand_widget', // Base ID
			esc_html__( 'Brand Widget', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'A Widget for all Brands', 'text_domain' ), ) // Args
		);		
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		$title = apply_filters('wpml_translate_single_string', $instance['title'], 'WooCommerce Minimum Quantity', 'Widget Title' );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} 
		$terms = get_terms( array(
		    'taxonomy' => 'brands',
		    'hide_empty' => true,
		    'parent'	=> 0
		) );

		$current_cat = get_queried_object()->term_id;
		$current_term = get_term( $current_cat, 'brands' );
		?>

		<?php if (!empty($terms) && ! is_wp_error( $terms )) : ?>
			<ul class="brand-categories">
				<?php foreach ( $terms as $term ) :
					$term_children = get_term_children($term->term_id, 'brands');

					$class = 'cat-item cat-item-'.$term->term_id;
					if ($current_cat == $term->term_id) {
						$class = $class . ' current-cat';
					}

				?>
					<li class="<?php echo $class; ?>">
						<a href="<?php echo get_term_link( $term->term_id) ?>">
							<?php echo $term->name; ?>

						</a>
						<?php if ( ((!empty($term_children)) && ($current_cat == $term->term_id)) || ($term->term_id == $current_term->parent)) : ?>
							<ul class="children">
								<?php foreach ($term_children as $term_child) : ?>
									<?php 
										$child = get_term_by( 'id', $term_child, 'brands' ); 
										$class = 'cat-item cat-item-'.$child->term_id;
										if ($current_cat == $child->term_id) {
											$class = $class . ' current-cat';
										}
									?>

									<li class="<?php echo $class; ?>">
										<a href="<?php echo get_term_link( $child->term_id) ?>">
											<?php echo $child->name; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php else : 
			echo __('No Brands to Show', 'Makeupcity');

		endif; ?>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'WooCommerce Minimum Quantity - Widget Title' );

		?>
		<!-- Title -->
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		//WMPL
		/**
		 * register strings for translation
		 */
		do_action( 'wpml_register_single_string', 'WooCommerce Minimum Quantity', 'Widget Title', $instance['title'] );
		//WMPL

		return $instance;
	}
} // class Logo_Widget


// register Logo_Widget widget
function register_brand_widget() {
    register_widget( 'Brand_Widget' );
}
add_action( 'widgets_init', 'register_brand_widget' );


/**
 * Include Jqyery Slider for Quantity.
 */
function min_quantity_slider() {

	if (is_active_widget( false, false, 'brand_widget', true )) {
		wp_enqueue_script( 'jquery-ui-slider' );
	}

}

add_action( 'wp_enqueue_scripts', 'min_quantity_slider' );