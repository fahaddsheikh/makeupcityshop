<?php

/**
 * Class for Brands
 */
class Brands
{
	
	function __construct()
	{
		add_action( 'init', array($this, 'brands_register'), 0 );

	}

	// Register Custom Post Type
	public function brands_register() {

		$taxonomy_labels = array(
			'name'                       => _x( 'Brands', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Brand', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Brand', 'text_domain' ),
			'all_items'                  => __( 'All Brands', 'text_domain' ),
			'parent_item'                => __( 'Parent Brand', 'text_domain' ),
			'parent_item_colon'          => __( 'Parent Brand:', 'text_domain' ),
			'new_item_name'              => __( 'New Brand Name', 'text_domain' ),
			'add_new_item'               => __( 'Add Brand Item', 'text_domain' ),
			'edit_item'                  => __( 'Edit Brand', 'text_domain' ),
			'update_item'                => __( 'Update Brand', 'text_domain' ),
			'view_item'                  => __( 'View Brand', 'text_domain' ),
			'separate_items_with_commas' => __( 'Separate brands with commas', 'text_domain' ),
			'add_or_remove_items'        => __( 'Add or remove brands', 'text_domain' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
			'popular_items'              => __( 'Popular brands', 'text_domain' ),
			'search_items'               => __( 'Search brands', 'text_domain' ),
			'not_found'                  => __( 'Not Found', 'text_domain' ),
			'no_terms'                   => __( 'No brands', 'text_domain' ),
			'items_list'                 => __( 'Brands list', 'text_domain' ),
			'items_list_navigation'      => __( 'Brands list navigation', 'text_domain' ),
		);
		$taxonomy_args = array(
			'labels'                     => $taxonomy_labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'brands', array( 'product' ), $taxonomy_args );
	}
}

new Brands();