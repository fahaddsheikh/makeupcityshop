<?php 
global $wp_query;
$big = 999999999; // need an unlikely integer
?>

<?php get_header(); ?>

<?php

$args = array(
	'post_type' => 'looks',
	'posts_per_page' => 4
);
$query = new WP_query ( $args );

if ( $query->have_posts() ) : ?>
	<div class="sober-banner-grid-4 ">
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>

			<?php if ($wp_query->current_post == 0) :
				$class = 'sober-banner light-scheme text-position-center button-visible-always';
			else :
				$class = 'sober-banner3 text-align-left height-300';
			endif; ?>
			<div class="<?php echo $class; ?>">
			    <a href="<?php echo get_the_permalink( get_the_ID() ) ?>">
			    	<img src="<?php echo get_the_post_thumbnail( get_the_ID(), 'medium' );?>" width="568" height="710">
			    </a>
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
<div class="clearfix"></div>
<?php if ( have_posts() ) : ?>
	<div id="primary" class="woocommerce content-area col-md-8 col-sm-12 col-xs-12" role="main">
		<ul class="products columns-5">
		    <?php while ( have_posts() ) : the_post(); ?>
		        <li id="post-<?php the_ID(); ?>" <?php post_class( array( 'col-md-4', 'col-sm-4', 'col-xs-6', 'col-lg-1-5', 'product-style-default' ) ); ?> class="">
		            <div class="product-header">
		                <a href="<?php echo get_permalink( ); ?>">
		                    <?php echo get_the_post_thumbnail( get_the_ID(), 'medium' );?>
		                	<img width="80" height="80" src="" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="">
		                </a>
		            </div>
		            <h3 class="woocommerce-loop-product__title">
		            	<a href="<?php echo get_permalink( ); ?>"><?php echo get_the_title(  ); ?></a>
		            </h3>
		        </li>
		    <?php endwhile; ?>
		</ul>
		<nav class="woocommerce-pagination">
		    <ul class="page-numbers">
		        <li><span aria-current="page" class="page-numbers current">1</span></li>
		        <li><a class="page-numbers" href="http://webmose.com/makeup/shop/page/2/">2</a></li>
		        <li><a class="page-numbers" href="http://webmose.com/makeup/shop/page/3/">3</a></li>
		        <li><a class="page-numbers" href="http://webmose.com/makeup/shop/page/4/">4</a></li>
		        <li><a class="page-numbers" href="http://webmose.com/makeup/shop/page/5/">5</a></li>
		        <li>
		            <a class="next page-numbers" href="http://webmose.com/makeup/shop/page/2/">
		                <svg viewBox="0 0 20 20">
		                    <use xlink:href="#right-arrow"></use>
		                </svg>
		            </a>
		        </li>
		    </ul>
		</nav>
		<?php 
		    echo paginate_links( array(
		        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		        'format' => '?paged=%#%',
		        'current' => max( 1, get_query_var('paged') ),
		        'total' => $wp_query->max_num_pages
		    ) );
		?>
	</div>
<?php endif; ?>

<?php get_sidebar('looks'); ?>
<?php get_footer(); ?>