// In your Javascript (external .js resource or <script> tag)
jQuery(document).ready(function($) {
    $('#billing_city').selectWoo({ width: '100%' });

    
    $(function(){
      $('#video').css({ width: $(window).innerWidth() + 'px', height: $(window).innerHeight() + 'px' });

      // If you want to keep full screen on window resize
      $(window).resize(function(){
        $('#video').css({ width: $(window).innerWidth() + 'px', height: $(window).innerHeight() + 'px' });
      });
    });

});