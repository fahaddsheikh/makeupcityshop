/**
 * Plugin Name: Woocommerce SMS
 * Description: woocommerce send sms notification to customer
 * Version: 2.9
 * Author: Onx Digital
 * Text Domain: onx-sms
 * License: GPL2
 *
*/

/*==============version 2.9 ================*/
Order dispatched to processing issue is fixed


/*==============version 2.8 ================*/
- New order status "Order Confirmed" registered
- New Order status will be processing and renamed its label to "Order Confirmation Pending"
- After clicking on SMS confirmation link order status will be changed into Order Confirmed


/*==============version 2.7 ================*/
- Emails sending nofications to customer is removed
- Order will be cancelled after certain day