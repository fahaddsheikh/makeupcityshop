<?php
/**
 * Plugin Name: Woocommerce SMS
 * Description: woocommerce send sms notification to customer
 * Version: 2.9
 * Author: Onx Digital
 * Text Domain: onx-sms
 * License: GPL2
 *
 */

if ( !defined( 'ABSPATH' ) ) exit;

define("ONX_SMS_ASSETS", plugin_dir_url( __FILE__ ).'assets/');

class ONX_WC_Order_SMS
{
    private $lifetimesms_is_enable;
    private $lifetimesms_api_token;
    private $lifetimesms_api_secret;
    private $lifetimesms_send_from;

    public function __construct()
    {
        $this->lifetimesms_is_enable 	=  get_option('wc_settings_onxsms_lifetimesms_on');
        $this->lifetimesms_api_token 	=  get_option('wc_settings_onxsms_lifetimesms_api_token');
        $this->lifetimesms_api_secret 	=  get_option('wc_settings_onxsms_lifetimesms_api_secret');
        $this->lifetimesms_send_from 	=  get_option('wc_settings_onxsms_lifetimesms_send_from');
        if ( $this->is_wc_active() ) {
            add_filter( 'woocommerce_settings_tabs_array', array($this,'add_onxsms_tab'), 50 );
            add_action( 'woocommerce_settings_tabs_onxsms', array($this, 'onxsms_settings') );
            add_action( 'woocommerce_update_options_onxsms', array($this, 'update_onxsms_settings' ));
            add_action( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'onxsms_plugin_action_links' ) );
            add_filter( 'wc_order_statuses', array($this, 'wc_onxsms_lifetimesms_order_status'));
            add_action( 'woocommerce_order_status_changed', array( $this, 'orderStatusUpdatedNotification'),10,1);
            add_action( 'woocommerce_thankyou_order_received_text', array($this, 'changing_order_status_after_confirmation'), 20, 2 );
            add_action( 'woocommerce_checkout_process', array($this, 'onxsms_checkout_field_process'));
            add_filter( 'woocommerce_cod_process_payment_order_status', array($this, 'set_cod_process_payment_order_status_on_hold'), 10, 2 );
            add_action( 'woocommerce_after_register_post_type', array($this, 'wc_onxsms_after_register_post_type') );
			add_action( 'init', array($this, 'wc_onxsms_lifetimesms_order_new_status'));
			add_action( 'admin_enqueue_scripts', array($this,'backend_register_scripts_styles'));
        }
    }
	
	public function backend_register_scripts_styles(){
		wp_enqueue_style( 'customstyles', ONX_SMS_ASSETS.'css/customstyles.css');			
	}
	
	public function wc_onxsms_lifetimesms_order_new_status(){
		register_post_status('wc-confirmed', array(
            'label'                     => 'Order Confirmed',
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop('Order Confirmed <span class="count">(%s)</span>', 'Confirm <span class="count">(%s)</span>')
        ));
	}

    public function wc_onxsms_after_register_post_type() {
        global $wpdb;
        $held_duration =  get_option( 'wc_settings_onxsms_validity_days' ); //get_option( 'woocommerce_hold_stock_minutes' );
        $held_duration = $held_duration * 86400;
        $post_time	= time() + $held_duration;
        $post_status = implode("','", array('wc-pending') );

        $results = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_type = 'shop_order' AND post_status IN ('{$post_status}') 
		AND post_modified <= NOW() - INTERVAL $held_duration MINUTE ");

        foreach($results as $result)
        {
            $order_id = $result->ID;
            $order = new WC_Order( $order_id );
            if($order)
            {
                $update_order = array(
                    'post_type' => 'shop_order',
                    'ID' => $order_id,
                    'post_status' => 'wc-cancelled'
                );
                $statusTest = wp_update_post($update_order);
            }
        }
    }

    public function onxsms_checkout_field_process(){
        // Add 92 as prefix to buyer phone number
        $number_length = strlen($_POST['billing_phone']);
        if($number_length == 11)
        {
            // Remove first char if it is 0 and add 92
            if($_POST['billing_phone'][0] == 0)
            {
                $_POST['billing_phone'] = substr($_POST['billing_phone'], 1);
                $_POST['billing_phone'] = '92'.$_POST['billing_phone'];
            }
        } elseif($number_length == 10)
        {
            $_POST['billing_phone'] = '92'.$_POST['billing_phone'];
        }
    }

    public function wc_onxsms_lifetimesms_order_status($order_statuses){
        $order_statuses['wc-processing'] = 'Order Confirmation Pending';
        return $order_statuses;
    }

    public function set_cod_process_payment_order_status_on_hold( $status, $order ) {
        $order_id  = $order->get_ID();
        if($this->lifetimesms_is_enable=='yes')
        {
            $order_status 	= $order->get_status();
            $activation_key = rand();
            update_post_meta($order_id, 'confirmation-key', $activation_key);
            $customer_name	= get_post_meta( $order_id, '_billing_first_name', true );

            $activation_link	= site_url().'/checkout/order-received/'.$order_id.'/?key='.$order->get_order_key().'&confirmation-key='.$activation_key;
            $message = "Hey $customer_name, Your order #$order_id has been received. Please visit this link to confirm your order at Makeup city. The order will not be processed until confirmed. ".$activation_link;
            $message =	urlencode($message);

            //$order->add_order_note( __($activation_link, 'onxsms' ) );
            $response = $this->sendSMS($order_id, $message, 'pending');
            if($response==300)
            {
                $order->add_order_note( __('Order '.$order_status.' SMS send to buyer successfully.', 'onxsms' ) );
            }
            return $status;
        }
    }

    public function changing_order_status_after_confirmation($thank_you_title, $order){
        $order_id = $order->get_ID();
        $thanks_style = '<style>
		.thanks-success{ color:#060;font-weight:600; }.thanks-error{ color:#E91B23;font-weight:500; }
		.woocommerce-notice.woocommerce-notice--success.woocommerce-thankyou-order-received { text-align: center; font-size: 15px; }
		</style>';

        $thanks_title = $thank_you_title;
		
		

        $activation_key = get_post_meta($order_id, 'confirmation-key', true);
        if(isset($_REQUEST['confirmation-key']) && $order->get_status()=='processing'){
            $validity_days  = get_option('wc_settings_onxsms_validity_days');
            $validity_days  = ($validity_days>0) ? $validity_days : 7;
            if($activation_key==$_REQUEST['confirmation-key']){
                $now = time();
                $order_date = strtotime($order->get_date_created());
                $no_of_days = $this->dateDifference($now, $order_date);
                if($no_of_days>$validity_days){
                    $thanks_title = '<span class="thanks-error">Your order confirmation key has been expired.</span>';
                } else {
                    $response = $this->update_order_status_stock($order);					
                    if($response) {
                        $thanks_title = $thanks_style.'<span class="thanks-success">Your order has been approved. Thanks for choosing us!</span>';
                    } else {
                        $thanks_title = $thanks_style.'<span class="thanks-error">Unfortunately the products you have ordered are currently out of stock. Please check the website again at a later time to shop your favorite products before they run out. </span>';
                    }
                }
            } else {
                if( $order->get_status() == 'order-confirmed' ) $thanks_title = $thanks_style.'<span class="thanks-success">Thank you so much for your order! 
				Your order is already approved.</span>';
            }
        } else {
            if(!empty($activation_key) && $order->get_status() == 'processing'){
                $thanks_title = $thanks_style.'<span class="thanks-success">We have received your order. An order confirmation link sent to you via text message. Please open the link to confirm your order.</span>';
            }
            if($activation_key == 'approved'){
                $thanks_title = $thanks_style.'<span class="thanks-success">Your order has been approved. Thanks for choosing us!</span>';
            }
        }
        return $thanks_title;
    }

    public function update_order_status_stock($order)
    {
        $order_id 		= $order->get_ID();		
		update_post_meta($order_id, 'confirmation-key', 'approved');
        $order->update_status( 'order-confirmed' );
		return true;
    }

    public function dateDifference($start_date, $end_date){
        $datediff = $start_date - $end_date;
        $num_days = ceil(abs($datediff / 86400));
        return $num_days;
    }

    private function is_wc_active()
    {
        if ( ! function_exists( 'is_plugin_active' ) ) { require_once( ABSPATH . '/wp-admin/includes/plugin.php' ); }
        if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) { $is_active = true; } else { $is_active = false;	}

        // Do the WC active check
        if ( false === $is_active ) {
            add_action( 'admin_notices', array( $this, 'notice_activate_wc' ) );
            deactivate_plugins( plugin_basename( __FILE__ ) );
        }
        return $is_active;
    }

    public function notice_activate_wc()
    {
        ?>
        <div class="error">
            <p><?php printf( __( 'Please install and activate %sWooCommerce%s for WooCommerce Order SMS!', 'woo-onx-sms' ),
                    '<a href="' . admin_url( 'plugin-install.php?tab=search&s=WooCommerce&plugin-search-input=Search+Plugins' ) . '">', '</a>' ); ?>
            </p>
        </div>
        <?php
    }

    public function add_onxsms_tab( $settings_tabs ) {
        $settings_tabs['onxsms'] = __( 'SMS', 'onxsms' );
        return $settings_tabs;
    }

    public function onxsms_settings() {
        woocommerce_admin_fields( self::get_onxsms_settings() );
    }

    public function update_onxsms_settings() {
        woocommerce_update_options( self::get_onxsms_settings() );
    }

    public function get_onxsms_settings() {
        $validity_days = get_option('wc_settings_onxsms_validity_days');
        $validity_days = ($validity_days>0) ? $validity_days : 7;
        $settings = array(
            'section_title' => array('name' => esc_html( 'SMS Settings', 'onxsms'), 'type' => 'title', 'id' => 'wc_settings_onxsms_section_title'),
            array('name' => esc_html( 'Enable', 'onxsms' ), 'type' => 'checkbox', 'id'  => 'wc_settings_onxsms_lifetimesms_on' ),
            array('name' => esc_html( 'Username', 'onxsms' ), 'type' => 'text', 'id'  => 'wc_settings_onxsms_lifetimesms_api_token' ),
            array('name' => esc_html( 'Password', 'onxsms' ), 'type' => 'text', 'id' => 'wc_settings_onxsms_lifetimesms_api_secret' ),
            array('name' => esc_html( 'Send From', 'onxsms'), 'type' => 'text', 'id' => 'wc_settings_onxsms_lifetimesms_send_from' ),
            array('name' => esc_html( 'Update Stock', 'onxsms'), 'type' => 'checkbox', 'id' => 'wc_settings_onxsms_update_stock',
                'desc' =>'Update product stock after SMS confirmation' ),
            array('name' => esc_html( 'SMS Validity Days', 'onxsms'), 'type' => 'text', 'id' => 'wc_settings_onxsms_validity_days',
                'desc' =>'SMS confirmation link valid for '.$validity_days.' days.'),
            'section_end' => array('type' => 'sectionend', 'id' => 'wc_settings_onxsms_section_end')
        );
        return apply_filters( 'wc_settings_onx_sms_settings', $settings );
    }

    public function orderStatusUpdatedNotification($order_id)
    {
        if($this->lifetimesms_is_enable=='yes'):
            $order  = wc_get_order($order_id);
            $order_status  = $order->get_status();
            $this->sendOrderNotificationBySMS($order_id, $order_status);
        endif;
    }

    public function sendSMS($order_id, $textMessage,$status)
    {
        $lifetimesms_api_token = $this->lifetimesms_api_token;
        $lifetimesms_api_secret = $this->lifetimesms_api_secret;
        $lifetimesms_send_from = $this->lifetimesms_send_from;
        $order  = wc_get_order($order_id);
        $customer_mobile  =  get_post_meta( $order_id, '_billing_phone', true );
        $url = "http://www.outreach.pk/api/sendsms.php/sendsms/url";
        $type	=	"xml";
        $lang	=	"English";
        $parameters = "id=".$lifetimesms_api_token."&pass=".$lifetimesms_api_secret."&msg=".$textMessage."&to=".$customer_mobile."&lang=".$lang."&mask=".$lifetimesms_send_from.
            "&type=".$type;
        $ch = curl_init();
        $timeout  =  30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $response = curl_exec($ch);
        $xmlRes = simplexml_load_string($response);
        curl_close($ch);
        return $xmlRes->code;
    }

    public function sendOrderNotificationBySMS($order_id, $new_status)
    {
        if( !$order_id ) { return; }
        $order 	= wc_get_order( $order_id );
        $customer_name	= get_post_meta( $order_id, '_billing_first_name', true );
        $response	= NULL;
		if($new_status=='order-confirmed'){
            $message = "Hey $customer_name, Your order #$order_id has been confirmed. Thank you for shopping at Makeup city.";
            $response = $this->sendSMS($order_id, $message, $new_status);
        } else if($new_status=='dispatched'){
            $message = "Hey $customer_name, Your order #$order_id has been dispatched. You will be receiving your order soon.";
            $response = $this->sendSMS($order_id, $message, $new_status);
        } else if($new_status=='cancelled'){
            $message = "Hey $customer_name, Your order #$order_id has been cancelled. It appears that we won't be needing it because you have not confirmed.";
            $response = $this->sendSMS($order_id, $message, $new_status);
        } else if($new_status=='on-hold'){
            $message = "Hey $customer_name, Your order #$order_id is on hold.";
            $response = $this->sendSMS($order_id, $message, $new_status);
        }
        if($response==300) $order->add_order_note( __('Order '.$new_status.' SMS send to buyer successfully.', 'onxsms' ) );
    }

    public function onxsms_plugin_action_links($links)
    {
        $links = array_merge(
            array('<a href="' . esc_url( admin_url( '/admin.php?page=wc-settings&tab=onxsms' ) ) . '">' . __( 'Settings' ) . '</a>'), $links
        );
        return $links;
    }
}

$smsObj	= new ONX_WC_Order_SMS;