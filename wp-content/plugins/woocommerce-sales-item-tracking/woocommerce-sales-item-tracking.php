<?php
/**
 * Plugin Name: Woocommerce Sales Item Tracking
 * Description: Invoice, Sales Price
 * Version: 1.5
 * Author: Onx Digital
 * Text Domain: woocommerce-sales-items-tracking
 * License: GPL2
 *
*/

defined('ABSPATH') || exit;
if (in_array('woocommerce-pip/woocommerce-pip.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    add_filter('wc_pip_document_table_headers', 'wsit_document_table_headers', 1, 3); // table column heading
    add_filter('wc_pip_document_sort_order_items_alphabetically', '__return_false');
    add_filter('wc_pip_show_print_dialog', '__return_false');
    add_filter('wc_pip_document_table_row_cells', 'wsit_filter_table_row_cells', 1, 6);
    add_filter('wc_pip_document_table_footer', 'wsit_pip_document_table_footer', 1, 3);
    add_action('woocommerce_thankyou', 'wsit_pip_thankyou', 10, 1);
    add_filter('woocommerce_hidden_order_itemmeta', 'wsit_remove_order_item_meta_fields');
	add_action('woocommerce_before_calculate_totals', 'changing_cart_item_prices', 20, 1 );
	add_action("woocommerce_order_item_after_calculate_taxes", "custom_order_item_after_calculate_taxes", 10, 2);
	if(!is_admin()) add_filter( 'woocommerce_calc_tax', 'filter_woocommerce_calc_tax', 10, 5 );
	
	add_filter( 'woocommerce_get_price_html', 'custom_price_html', 100, 2 );
	add_filter( 'woocommerce_variable_price_html', 'custom_price_html', 10, 2 );
		
	function custom_price_html($price_html, $product){		
		$tax_rates  	  = WC_Tax::get_rates( $product->get_tax_class() );
		$tax_rates  	  = $tax_rates[1]['rate'];
		$currency_symbol  = get_woocommerce_currency_symbol();
		
		$newhtmlprice     = '';
		if ($product->is_type('variable')) {
			
			if($product->is_on_sale()){
				// Regular price min
				$min_regular_price = $product->get_variation_regular_price( 'min' );
				$max_regular_price = $product->get_variation_regular_price( 'max' );
				
				$min_sale_price = $product->get_variation_sale_price( 'min' );
				$max_sale_price = $product->get_variation_sale_price( 'max' );
				
				$tax_min_regular_price 	= ($min_regular_price * $tax_rates)/100;
				$new_min_regular_price  = round($min_regular_price + $tax_min_regular_price);
					
				// Regular price max			
				$tax_max_regular_price 	= ($max_regular_price * $tax_rates)/100;
				$new_max_regular_price  = round($max_regular_price + $tax_max_regular_price);
				
				// Sale price min and max
				$new_min_sale_price  = round($min_sale_price + $tax_min_regular_price);
				
				if($min_sale_price!=$max_sale_price){				
					$price_html = '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.$currency_symbol.'</span>'.$new_min_sale_price.'</span> – <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.$currency_symbol.'</span>'.$new_max_regular_price.'</span>';	
				} else {
					$price_html = '<p class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.$currency_symbol.'</span>'.$new_max_regular_price.'</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.$currency_symbol.'</span>'.$new_min_sale_price.'</span></ins></p>';
				}					
				$newhtmlprice = $price_html;
			} else {
				$newhtmlprice = $price_html;
			}
		} else {
			// Main Price
			if($product->is_on_sale()){
				$regular_price = $product->get_regular_price();
    			$sale_price = $product->get_sale_price();		
				$tax_regular_price 	= ($regular_price * $tax_rates)/100;
				$new_regular_price  = round($regular_price + $tax_regular_price);
				$new_sale_price     = round($sale_price + $tax_regular_price);
				$newhtmlprice = '<del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.$currency_symbol.'</span>
				'.$new_regular_price.'</span></del>&nbsp;<ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.$currency_symbol.'
				</span>'.$new_sale_price.'</span></ins>';
			} else {
				$newhtmlprice = $price_html;
			}
		}
		return $newhtmlprice;
	}
	
	function filter_woocommerce_calc_tax( $taxes, $price, $rates, $price_includes_tax, $suppress_rounding ) { 
		global $woocommerce;
		if ( is_cart() || is_checkout() ) {
			$price_after_tax = 0;	
			$tax_rate 	= $rates[1]['rate'];
			$items 		= $woocommerce->cart->get_cart();
			$wc_cart_tax = 0;
			foreach ($items as $item) {
				$item_id            = $item['data']->get_id();
				$product 			 = wc_get_product($item_id);
				$regular_price		 = $product->get_regular_price();
				$product_quantity    = $item['quantity'];
				$price_with_tax    	 = ($regular_price * $product_quantity * $tax_rate)/100;
				$wc_cart_tax         = $wc_cart_tax + $price_with_tax;
			}
			if(count($items)>0) $wc_cart_tax= $wc_cart_tax/count($items);		
			$taxes[1]=$wc_cart_tax;
		}
		return $taxes; 
	}	
    function custom_order_item_after_calculate_taxes($order, $calculate_tax_for) {		
		if ( '0' !== $order->get_tax_class() && 'taxable' === $order->get_tax_status() && wc_tax_enabled() ) {
			$calculate_tax_for['tax_class'] = $order->get_tax_class();
			$tax_rates                      = WC_Tax::find_rates( $calculate_tax_for );
			$taxes                          = WC_Tax::calc_tax( $order->get_total(), $tax_rates, false );
			if ( method_exists( $order, 'get_subtotal' ) ) {
				$order_price	= $order->get_subtotal();
				$tax_rate 	= $tax_rates[1]['rate'];
				$GST_Tax    	 = ($order_price * $tax_rate)/100;
				$subtotal_taxes = WC_Tax::calc_tax( $order->get_subtotal(), $tax_rates, true );
				$subtotal_taxes[1] = $GST_Tax;
				$order->set_taxes(
					array(
						'total'    => $subtotal_taxes,
						'subtotal' => $subtotal_taxes,
					)
				);
			} else {
				$order->set_taxes( array( 'total' => $taxes ) );
			}
		} else {
			$order->set_taxes( false );
		}
    }
	function changing_cart_item_prices( $cart ) {
    	if ( is_admin() ) return false;
	}    
    function wsit_wc_pip_document_column_widths($column_widths, $order_id, $type)
    {
        $column_widths['total'] = 30;
        return $column_widths;
    }
    function wsit_pip_document_table_footer($rows, $type, $order_id)
    {
        if ($type == 'invoice'):
			$payment_method         = $rows['payment_method'];
            $order_total            = $rows['order_total'];
			unset($rows['discount']);
            unset($rows['payment_method']);
            unset($rows['order_total']);
            $wsit_order        = wc_get_order($order_id);
			$discount_total		= $wsit_order->get_discount_total();						
			if($discount_total>0){
				$discount_total  = wc_price($discount_total, array( 'currency' => $wsit_order->get_currency() ));
				$rows['coupon'] = array('coupon'=>'<strong class="order-discount">Discount:</strong>', 'value'=>'-'.$discount_total);
			}
			$rows['payment_method'] = $payment_method;
            $rows['order_total']    = $order_total;
            $subtotal 				= get_wsit_subtotal($order_id);
            $rows['cart_subtotal']['value'] = $subtotal;
        endif;
        return $rows;
    }
    function wsit_filter_table_row_cells($items, $type, $item_id, $item, $product, $order)
    {
        $quantity           = $items['quantity'];
        unset($items['quantity']);
        $items['quantity']  = $quantity;
        $total_price        = get_regular_price_by_item_id($order, $item_id);
        $total_price        =   wc_price($total_price, array( 'currency' => $order->get_currency() ));
        $order_items        = $order->get_items('coupon');
		$coupon_codes       = '';
        if ($order->get_coupon_codes()) {
            foreach ($order->get_coupon_codes() as $coupon) {
                $coupon_code =  $coupon;
            }
        }
		$item_price     =   get_item_price_by_item_id($order, $item_id);
		$disc_amt       =   get_item_disc_by_item_id($order, $item_id,$coupon_codes);
        if ($type=='invoice') {
            $items['price'] = $item_price;
            $items['discount'] = $disc_amt;
            $items['total'] = $total_price;
        }
        return $items;

    }
    function get_wsit_subtotal($order_id)
    {
        $subtotal           =   0;
        $wsit_order         =   wc_get_order($order_id);
        $items              =   $wsit_order->get_items();
        foreach ($items as $item) {
            $order_item_id  =   $item->get_id();
            $line_total     =   wc_get_order_item_meta($order_item_id, '_line_subtotal', true);
            $subtotal       =   $subtotal + $line_total;
        }
        $subtotal       =   wc_price($subtotal, array( 'currency' => $wsit_order->get_currency() ));
        return $subtotal;
    }
    function wsit_pip_thankyou($order_id)
    {
		global $wpdb;
		$t_regular_price 	= 	0;
        $t_sale_price 		= 	0;
		$t_sales_discount 	= 	0;
        $order          	= 	wc_get_order($order_id);
		$total_tax      	= 	$order->get_total_tax();
		$items          	= 	$order->get_items();
		$discount_total		=	$order->get_discount_total();
		if (!empty($total_tax)) {
            foreach($order->get_items('tax') as $item): $rate_percent =  $item->get_rate_percent(); endforeach;
        }
		foreach ($items as $item) {
			$order_item_id  = $item->get_id();
            $product_id     = $item->get_product_id();
            $item_data 		= $item->get_data();
			$product        = wc_get_product($product_id);
			if ($product->is_type('variable')){
				$variation_id 	   = $item->get_variation_id();
				$variation_product = wc_get_product($variation_id);
				if($variation_product->is_on_sale()){
					$order_item_id   = $item->get_id();
					$quantity        = $item->get_quantity();
					$regular_price   = $variation_product->get_regular_price();
					$sale_price	     = $variation_product->get_sale_price();
										
					$t_regular_price = $regular_price * $quantity;
                	$t_sale_price    = $sale_price * $quantity;
					
					wc_update_order_item_meta($order_item_id, 'Sale item', 'Yes');
					wc_update_order_item_meta($order_item_id, '_regular_price', $regular_price);
					wc_update_order_item_meta($order_item_id, '_sale_price', $sale_price);
					
					$sales_discount		= $t_regular_price - $t_sale_price;
					$t_sales_discount = $t_sales_discount + $sales_discount;
					
				} else {
					$regular_price   = $variation_product->get_regular_price();
					wc_update_order_item_meta($order_item_id, '_regular_price', $line_subtotal);
				}
			} else {
				if ($product->is_on_sale()){
					$order_item_id   = $item->get_id();
					$quantity        = $item->get_quantity();
					$regular_price   = $product->get_regular_price();
					$sale_price      = $product->get_sale_price();					
					$t_regular_price = $regular_price * $quantity;
					$t_sale_price    = $sale_price * $quantity;
					wc_update_order_item_meta($order_item_id, 'Sale item', 'Yes');
					wc_update_order_item_meta($order_item_id, '_regular_price', $regular_price);
					wc_update_order_item_meta($order_item_id, '_sale_price', $sale_price);
					$sales_discount		= $t_regular_price - $t_sale_price;
					$t_sales_discount = $t_sales_discount + $sales_discount;
					
				} else {
					$regular_price   = $product->get_regular_price();
					wc_update_order_item_meta($order_item_id, '_regular_price', $regular_price);
				}
			}
		}
		
		$total_sales_discount = get_post_meta( $order_id, 'sales_discount', true);
		if(empty($total_sales_discount)){
			if($t_sales_discount>0){
				//update_post_meta( $order_id, 'sales_discount', $t_sales_discount);
				$total_discount = $discount_total + $t_sales_discount;
				$order->set_discount_total($total_discount);
				$order->save();				
			}
		}
	}
    function wsit_remove_order_item_meta_fields($fields)
    {
        $fields[] = '_regular_price';
        $fields[] = '_sale_price';
        $fields[] = '_discount_item';
        return $fields;
    }
    function get_regular_price_by_item_id($wsit_order, $order_item_id)
    {
        $wsit_regular_price = '';
        $line_total = wc_get_order_item_meta($order_item_id, '_line_total', true);
        if ($line_total>0) {
            $wsit_regular_price = $line_total;
        }
        return $wsit_regular_price;
    }
    
    function get_item_price_by_item_id($wsit_order, $order_item_id)
    {
        $wsit_item_price = 0;
        $is_on_sale     = wc_get_order_item_meta($order_item_id, 'Sale item', true);
        if ($is_on_sale=='Yes') {
            $regular_price      =   wc_get_order_item_meta($order_item_id, '_regular_price', true);
            $regular_price      =   wc_price($regular_price, array( 'currency' => $wsit_order->get_currency() ));
                            
            $sale_price         =   wc_get_order_item_meta($order_item_id, '_sale_price', true);
            $sale_price         =   wc_price($sale_price, array( 'currency' => $wsit_order->get_currency() ));
            $wsit_item_price    =  '<del>'.$regular_price.'</del> '.$sale_price;
        } else {
            $qty                =   wc_get_order_item_meta($order_item_id, '_qty', true);
            $qty                =   (int) $qty;
            $line_total         =   wc_get_order_item_meta($order_item_id, '_line_total', true);
            $coupon_item        =   wc_get_order_item_meta($order_item_id, 'Coupon item', true);
            if ($line_total>0) {
                if ($coupon_item=='Yes') {
                    $line_subtotal      =   wc_get_order_item_meta($order_item_id, '_line_subtotal', true);
                    $line_subtotal      =   $line_subtotal/$qty;
                    $line_subtotal      =   wc_price($line_subtotal, array( 'currency' => $wsit_order->get_currency() ));
                    $wsit_item_price    =   $line_subtotal;
                } else {
                    $item_price         =   $line_total/$qty;
                    $item_price         =   wc_price($item_price, array( 'currency' => $wsit_order->get_currency() ));
                    $wsit_item_price    =   $item_price;
                }
            }
        }
        return $wsit_item_price;
    }
    function get_item_disc_by_item_id($wsit_order, $order_item_id, $coupon_code)
    {
        $wsit_disc_price        = 0;
		$wsit_item_price		= '';
        $coupon_item            = wc_get_order_item_meta($order_item_id, 'Coupon item', true);
        $is_on_sale             = wc_get_order_item_meta($order_item_id, 'Sale item', true);
        $qty                    = wc_get_order_item_meta($order_item_id, '_qty', true);
        $qty                    = (int) $qty;
        
        if ($coupon_item=='Yes') {
            $line_subtotal      =   wc_get_order_item_meta($order_item_id, '_line_subtotal', true);
            $line_total         =   wc_get_order_item_meta($order_item_id, '_line_total', true);
            $coupon_discount    =   $line_subtotal - $line_total;
            $coupon_discount    =   $coupon_discount;
            $coupon_discount    =   wc_price($coupon_discount, array( 'currency' => $wsit_order->get_currency() ));
            $wsit_item_price    =   $coupon_discount;
        }
        
        if ($is_on_sale=='Yes') {
            $regular_price      =   wc_get_order_item_meta($order_item_id, '_regular_price', true);
            $sale_price         =   wc_get_order_item_meta($order_item_id, '_sale_price', true);
            $wsit_disc_price    =   $regular_price - $sale_price;
            $wsit_disc_price    =   $wsit_disc_price * $qty;
            $wsit_disc_price    =   wc_price($wsit_disc_price, array( 'currency' => $wsit_order->get_currency() ));
            $wsit_item_price    =   $wsit_disc_price;
        }
        return $wsit_item_price;
    }
    
    function wsit_sales_discount($order_id)
    {
        $order              = wc_get_order($order_id);
        $items              = $order->get_items();
        $total_discount = 0;
        foreach ($items as $item) {
            $order_item_id      =   $item->get_id();
            $line_subtotal      =   wc_get_order_item_meta($order_item_id, '_line_subtotal', true);
            $line_total         =   wc_get_order_item_meta($order_item_id, '_line_total', true);
            $coupon_discount    =   $line_subtotal - $line_total;
            $total_discount     =   $total_discount + $coupon_discount;
        }
		return $total_discount;
    }
    
    function wsit_document_table_headers($table_headers, $order_id, $type)
    {
        if ($type=='invoice') :
            unset($table_headers['quantity']);
            $table_headers['quantity']  = 'Quantity';
            $table_headers['discount']  = 'Discount';
            $table_headers['total']     = 'Total';
        endif;
        return $table_headers;
    }
}