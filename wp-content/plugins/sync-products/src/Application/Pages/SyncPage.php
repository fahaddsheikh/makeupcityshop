<?php
namespace SyProducts\Application\Pages;

use SyProducts\Application\Apis\SyncPrice;
use SyProducts\Application\Apis\SyncSales;
use SyProducts\Application\Apis\SyncStock;

/**
 * 
 */
class SyncPage
{
	
	function __construct()
	{
		add_menu_page( 'Sync Products Settings', 'Sync Products Settings', 'manage_options', 'sync-products', array($this,'SyncTestCallback'), '', null );
	}

	public function SyncTestCallback ()
	{
		if(isset($_POST['update_syncEmails'])) {
			update_option( 'sync_emails', sanitize_text_field($_POST['sync_emails']));
		}
		if(isset($_POST['sync_products_stock'])) {
            $this->syncStock();
        }
        if(isset($_POST['sync_products_price'])) {
            $this->syncPrice();
        }
        if(isset($_POST['sync_sales'])) {
            $this->syncSales();
        }
		echo "<h2>Enter emails coma separated</h2>";
		echo "<div class='emails'>";
		echo "<form action='' method='POST'>";
		echo "<input type='text' name='sync_emails' value='".esc_attr(get_option('sync_emails'))."'>";
		echo "<input type='submit' name='update_syncEmails' value='Update Options' class='button primary'>";
		echo "</form>";
		echo "</div>";


        echo "<h2>Sync Manually Stock</h2>";
        echo "<div class='sync_manually'>";
        echo "<form action='' method='POST'>";
        echo "<input type='submit' name='sync_products_stock' value='Sync Products Stock' class='button primary'>";
        echo "</form>";
        echo "</div>";

        echo "<h2>Sync Manually Price</h2>";
        echo "<div class='sync_manually'>";
        echo "<form action='' method='POST'>";
        echo "<input type='submit' name='sync_products_price' value='Sync Products Price' class='button primary'>";
        echo "</form>";
        echo "</div>";

        echo "<h2>Sync Sales Manually</h2>";
        echo "<div class='sync_manually'>";
        echo "<form action='' method='POST'>";
        echo "<input type='submit' name='sync_sales' value='Sync Sales' class='button primary'>";
        echo "</form>";
        echo "</div>";
	}

    public function syncStock()
    {
        $syncStockPrice = new SyncStock();
        $syncStockPrice->updateProducts();
	}

    public function syncSales()
    {
        $syncSales = new SyncSales();
        $syncSales->updateProducts();
    }

    public function syncPrice()
    {
        $syncStockPrice = new SyncPrice();
        $syncStockPrice->updateProducts();
    }
}