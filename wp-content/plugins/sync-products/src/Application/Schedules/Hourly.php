<?php 
namespace SyProducts\Application\Schedules;

use SyProducts\Application\Apis\SyncPrice;
use SyProducts\Application\Apis\SyncSales;
use SyProducts\Application\Apis\SyncStock;

/**
 * 
 */
class Hourly
{
    public function syncHourlyData()
    {
        return false;
        $syncStock = new SyncStock();
        $syncStock->updateProducts();

        $syncPrice = new SyncPrice();
        $syncPrice->updateProducts();

        $syncSales = new SyncSales();
        $syncSales->updateProducts();
    }
}