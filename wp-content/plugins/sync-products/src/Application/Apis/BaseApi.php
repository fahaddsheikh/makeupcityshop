<?php


namespace SyProducts\Application\Apis;


use Katzgrau\KLogger\Logger;
use Psr\Log\LogLevel;

class BaseApi
{
    public function getApiData($url,$method,$request_data="")
    {
        $remote_data_arr = [];
        if($method === "GET") {
            $remote_data_arr = $this->getFromRemote($url,$request_data);
        }
        if($method==="POST") {
            $remote_data_arr = $this->postToRemote($url,$request_data);
        }
        return $remote_data_arr;
    }

    public function postToRemote($url,$request_data="")
    {
        $logger = new Logger($this->getLogsDirPath(),LogLevel::INFO,array('filename'=>date("H")));
        $response = wp_remote_post( $url, $request_data );
//		$logger->info(print_r($response, true));
        if(is_wp_error($response)) {
            $error_message = $response->get_error_message();
            $logger->info("Something went wrong: ".$error_message);
            return [];
        }

        return json_decode($response['body']);
    }

    public function getFromRemote($url,$request_data="")
    {
        $logger = new Logger($this->getLogsDirPath(),LogLevel::INFO,array('filename'=>date("H")));
        $response = wp_remote_get( $url, $request_data );
//		$logger->info(print_r($response, true));
        if(is_wp_error($response)) {
            $error_message = $response->get_error_message();
            $logger->info("Something went wrong: ".$error_message);
            return [];
        }
        return json_decode($response['body']);
    }

    public function findRemoteProductByBarcode($bar_code,$all_products)
    {
        $found_product = false;
        if(!empty($all_products)) {
            $allBarcodes = array_column($all_products, "Barcode");
            $index     =    array_search($bar_code, $allBarcodes);
            if($index !== false) {
                $found_product = $all_products[$index];
            }
        }
        return $found_product;
    }

    public function calculateSalePercentagePriceOnRegularPrice($regular_price, $percentage)
    {
        $sale_price = false;
        if((float)$regular_price && (float) $percentage) {
            $sale_price = number_format(($percentage / 100) * $regular_price,2);
        }
        return $sale_price;
    }

    public static function getLogsDirPath()
    {
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $logs_dir_path = $upload_dir.'/sync-products/inventorylogs/'.date("Y-m-d").'/';
        return $logs_dir_path;
    }

    public static function getPriceLogsDirPath()
    {
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $logs_dir_path = $upload_dir.'/sync-products/pricelogs/'.date("Y-m-d").'/';
        return $logs_dir_path;
    }

    public static function getSalesLogsDirPath()
    {
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $logs_dir_path = $upload_dir.'/sync-products/salelogs/'.date("Y-m-d").'/';
        return $logs_dir_path;
    }
}