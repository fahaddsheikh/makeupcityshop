<?php


namespace SyProducts\Application\Apis;


use Katzgrau\KLogger\Logger;
use Psr\Log\LogLevel;

class SyncStock extends BaseApi
{
    const API_URL = "https://tsminingapi.azurewebsites.net/TSBE/BranchWiseInventory/GetBranchWiseInventoryForWeb?LoginId=superUser&Password=techSupport20177&AppKey=mehran&CompanyBranchIds=2044&Barcode=";

    const API_METHOD = "POST";
    private $remote_products = array();

    public function updateProducts()
    {
        $this->remote_products = $this->fetchData();
        $this->updateLocalProducts();
    }

    public function fetchData()
    {
        $api_url = self::API_URL;

        return $this->getApiData($api_url,self::API_METHOD);

    }

    public function updateLocalProducts()
    {
        $local_products = array();
        $query = new \WC_Product_Query( array(
            'limit' => -1,
            'status' => array('publish'),
            'return' => 'ids',
        ) );
        $local_products = $query->get_products();


        $total_stock_updates = 0;
        $total_products_out_of_stock = 0;
        $total_products_in_stock = 0;
        $total_remote_matched_products = 0;
        $total_remote_products = is_countable($this->remote_products) ? count($this->remote_products) : 0;
        $total_local_products = is_countable($this->remote_products) ? count($local_products) : 0;


        $logger = new Logger($this->getLogsDirPath(),LogLevel::INFO,array('filename'=>date("H")));
        $logger->info('======================================'.date("Y-m-d H:i:s").'============================================');
        $logger->info('=========================================================================================================');
        $logger->info('=======================================Logs of the Hour==================================================');

        foreach ($local_products as $product_id) {
            $product = wc_get_product($product_id);

            if( $product->is_type( 'variable' ) ){
                $product = new \WC_Product_Variable( $product_id );
                $variations = $product->get_available_variations();
                foreach ($variations as $variation) {
                    $product = wc_get_product($variation['variation_id']);
                    $sku_str = $product->get_sku();
                    $remote_product_obj = $this->findRemoteProductByBarcode($sku_str,$this->remote_products);
                    if(!$remote_product_obj) {
                        $logger->info('['.$sku_str.'] - ['.$product->get_name().'] Not found in API Response');
                        continue;
                    }
                    $total_remote_matched_products++;

                    $stock_status = $this->syncTheProduct($product, $remote_product_obj, $sku_str, $logger);
                    if($stock_status['in_stock']) {
                        $total_products_in_stock++;
                    } else {
                        $total_products_out_of_stock++;
                    }
                    if($stock_status['stock_update']=== true) {
                        $total_stock_updates++;
                    }
                }
            } else {
                $product = wc_get_product($product_id);
                $sku_str = $product->get_sku();
                $remote_product_obj = $this->findRemoteProductByBarcode($sku_str,$this->remote_products);
                if(!$remote_product_obj) {
                    $logger->info('['.$sku_str.'] - ['.$product->get_name().'] Not found in API Response');
                    continue;
                }
                $total_remote_matched_products++;
                $stock_status = $this->syncTheProduct($product, $remote_product_obj, $sku_str, $logger);
                if($stock_status['in_stock']) {
                    $total_products_in_stock++;
                } else {
                    $total_products_out_of_stock++;
                }
                if($stock_status['stock_update']=== true) {
                    $total_stock_updates++;
                }
            }
        }

        $logger->info('=======================================SYNC SUMMARY==================================================');


        $logger->info('Total Products with Stock Updates: '.$total_stock_updates);
        $logger->info('Total Marked Out Of Stock: '.$total_products_out_of_stock);
        $logger->info('Total Marked In Stock: '.$total_products_in_stock);
        $logger->info('Total Remote Products: '.$total_remote_products);
        $logger->info('Total Local Products: '.$total_local_products);
        $logger->info('Total Remote Matched with Local Products: '.$total_remote_matched_products);

        $logger->info('=========================================================================================');
        $logger->info('=========================================================================================');
        $logger->info('=========================================================================================');
    }

    public function syncTheProduct($local_product_obj, $remote_product_obj, $local_sku_str, $logger)
    {
        $in_stock = false;
        $stock_update = false;
        $product_id = $local_product_obj->get_id();
        $local_product_obj->set_manage_stock(true);
        $local_qty = ($local_product_obj->get_stock_quantity()) ? $local_product_obj->get_stock_quantity() : 0;
        $product_local_price = $local_product_obj->get_regular_price();
        $remote_qty = $remote_product_obj->Inventory;
        $product_remote_price = $remote_product_obj->SaleRate;
        if((int)$local_qty !== (int)$remote_qty) {
            $stock_update = true;

            if((int)$remote_qty > 0) {
                $in_stock = true;
                $local_product_obj->set_stock_quantity($remote_qty);
                $local_product_obj->set_stock_status('instock');
            } else {
                $products_arr_out_of_stock[] = $product_id;
                $local_product_obj->set_stock_quantity(0);
                $local_product_obj->set_stock_status('outofstock');
            }
            if((int)$remote_qty < 0) {
                $remote_qty = 0;
            }
            $logger->info('['.$local_sku_str.'] - ['.$local_product_obj->get_name().'] - Stock updated: ['.$local_qty.'] to ['.$remote_qty.']');

        } else {
            $logger->info('['.$local_sku_str.'] - ['.$local_product_obj->get_name().'] - Stock remained the same: ['.$remote_qty.'] to ['.$local_qty.']');
        }
        $local_product_obj->save();

        return [
            'in_stock' => $in_stock,
            'stock_update' => $stock_update
        ];
    }
}