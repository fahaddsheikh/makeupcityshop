<?php


namespace SyProducts\Application\Apis;


use Katzgrau\KLogger\Logger;
use Psr\Log\LogLevel;

class SyncPrice extends BaseApi
{
    const API_URL = "https://tsminingapi.azurewebsites.net/TSBE/BranchWiseInventory/GetBranchWiseInventoryForWeb?LoginId=superUser&Password=techSupport20177&AppKey=mehran&CompanyBranchIds=2044&Barcode=";

    const API_METHOD = "POST";
    private $remote_products = array();

    public function updateProducts()
    {
        $this->remote_products = $this->fetchData();
        $this->updateLocalProducts();
    }

    public function fetchData()
    {
        $api_url = self::API_URL;

        return $this->getApiData($api_url,self::API_METHOD);

    }

    public function updateLocalProducts()
    {
        $local_products = array();
        $query = new \WC_Product_Query( array(
            'limit' => -1,
            'status' => array('publish'),
            'return' => 'ids',
        ) );
        $local_products = $query->get_products();

        $total_products_price_updated = 0;
        $total_remote_matched_products = 0;
        $total_remote_products = is_countable($this->remote_products) ? count($this->remote_products) : 0;
        $total_local_products = is_countable($this->remote_products) ? count($local_products) : 0;


        $logger = new Logger($this->getPriceLogsDirPath(),LogLevel::INFO,array('filename'=>date("H")));
        $logger->info('======================================'.date("Y-m-d H:i:s").'============================================');
        $logger->info('=========================================================================================================');
        $logger->info('=======================================Logs of the Hour==================================================');

        foreach ($local_products as $product_id) {
            $product = wc_get_product($product_id);

            if( $product->is_type( 'variable' ) ){
                $product = new \WC_Product_Variable( $product_id );
                $variations = $product->get_available_variations();
                foreach ($variations as $variation) {
                    $product = wc_get_product($variation['variation_id']);
                    $sku_str = $product->get_sku();
                    $remote_product_obj = $this->findRemoteProductByBarcode($sku_str,$this->remote_products);
                    if(!$remote_product_obj) {
                        $logger->info('['.$sku_str.'] - ['.$product->get_name().'] Not found in API Response');
                        continue;
                    }
                    $total_remote_matched_products++;

                    $is_updated = $this->syncTheProduct($product, $remote_product_obj, $sku_str, $logger);
                    if($is_updated['price_updated']) {
                        $total_products_price_updated++;
                    }
                }
            } else {
                $product = wc_get_product($product_id);
                $sku_str = $product->get_sku();
                $remote_product_obj = $this->findRemoteProductByBarcode($sku_str,$this->remote_products);
                if(!$remote_product_obj) {
                    $logger->info('['.$sku_str.'] - ['.$product->get_name().'] Not found in API Response');
                    continue;
                }
                $total_remote_matched_products++;
                $is_updated = $this->syncTheProduct($product, $remote_product_obj, $sku_str, $logger);
                if($is_updated['price_updated']) {
                    $total_products_price_updated++;
                }
            }
        }

        $logger->info('=======================================SYNC SUMMARY==================================================');


        $logger->info('Total Products with Price Updates: '.$total_products_price_updated);
        $logger->info('Total Remote Products: '.$total_remote_products);
        $logger->info('Total Local Products: '.$total_local_products);
        $logger->info('Total Remote Matched with Local Products: '.$total_remote_matched_products);

        $logger->info('=========================================================================================');
        $logger->info('=========================================================================================');
        $logger->info('=========================================================================================');
    }

    public function syncTheProduct($local_product_obj, $remote_product_obj, $local_sku_str, $logger)
    {
        $product_local_price = $local_product_obj->get_regular_price();
        $product_remote_price = $remote_product_obj->SaleRate;
        $price_update = false;
        if($product_local_price != $product_remote_price) {
            $local_product_obj->set_regular_price($product_remote_price);
            $price_update = true;
            $logger->info('['.$local_sku_str.'] - ['.$local_product_obj->get_name().'] - Price updated: ['.$product_local_price.'] to ['.$product_remote_price.']');

        } else {
            $logger->info('['.$local_sku_str.'] - ['.$local_product_obj->get_name().'] - Price remained the same: ['.$product_remote_price.'] to ['.$product_local_price.']');
        }
        $local_product_obj->save();

        return [
            'price_updated' => $price_update
        ];
    }
}