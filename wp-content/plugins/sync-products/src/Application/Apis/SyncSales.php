<?php


namespace SyProducts\Application\Apis;


use Katzgrau\KLogger\Logger;
use Psr\Log\LogLevel;

class SyncSales extends BaseApi
{
    const API_URL = "https://tsminingapi.azurewebsites.net/TSBE/ProductDiscount/GetProductDiscountData?LoginId=superUser&Password=techSupport20177&AppKey=mehran&CompanyBranchId=2044";
    const API_METHOD = "GET";
    private $remote_products = [];

    public function updateProducts()
    {
        $this->remote_products = $this->fetchData();
        $this->updateLocalProducts();
    }

    public function fetchData()
    {
        $api_url = self::API_URL."&startDate=2021/06/06&endDate=2021/06/30&Barcode=";

        return $this->getApiData($api_url,self::API_METHOD);

    }

    public function updateLocalProducts()
    {
        $query = new \WC_Product_Query( array(
            'limit' => -1,
            'status' => array('publish'),
            'return' => 'ids',
        ) );
        $local_products = $query->get_products();
        $total_local_products = 0;
        $total_remote_matched_products = 0;
        $total_products_added_to_sale = 0;
        $logger = new Logger($this->getSalesLogsDirPath(),LogLevel::INFO,array('filename'=>date("H")));
        $logger->info('======================================'.date("Y-m-d H:i:s").'============================================');
        $logger->info('=========================================================================================================');
        $logger->info('=======================================Logs of the Hour==================================================');

        foreach ($local_products as $product_id) {
            $total_local_products++;
            $product = wc_get_product( $product_id);

            if( $product->is_type( 'variable' ) ){
                $product = new \WC_Product_Variable( $product_id );
                $variations = $product->get_available_variations();
                foreach ($variations as $variation) {
                    $product = wc_get_product($variation['variation_id']);
                    $sku_str = $product->get_sku();
                    $remote_product_obj = $this->findRemoteProductByBarcode($sku_str,$this->remote_products);
                    if(!$remote_product_obj) {
                        $logger->info('['.$sku_str.'] - ['.$product->get_name().'] Not found in API Response');
                        continue;
                    }
                    $total_remote_matched_products++;

                    $sale_status = $this->syncTheProduct($product, $remote_product_obj, $sku_str, $logger);
                    if($sale_status['product_on_sale'] == true) {
                        $total_products_added_to_sale++;
                    }
                }
                continue;
            } else {
                $product = wc_get_product($product_id);
                $sku_str = $product->get_sku();
                $remote_product_obj = $this->findRemoteProductByBarcode($sku_str,$this->remote_products);
                if(!$remote_product_obj) {
                    $logger->info('['.$sku_str.'] - ['.$product->get_name().'] Not found in API Response');
                    continue;
                }
                $total_remote_matched_products++;
                $sale_status = $this->syncTheProduct($product, $remote_product_obj, $sku_str, $logger);
                if($sale_status['product_on_sale'] == true) {
                    $total_products_added_to_sale++;
                }
            }

        }
        $logger->info('=======================================SYNC SUMMARY==================================================');

        $logger->info('Total Products on Sale: '.$total_products_added_to_sale);
        $logger->info('Total Remote Products: '.count($this->remote_products));
        $logger->info('Total Local Products: '.$total_local_products);
        $logger->info('Total Remote Matched with Local Products: '.$total_remote_matched_products);

        $logger->info('=========================================================================================');
        $logger->info('=========================================================================================');
        $logger->info('=========================================================================================');
    }

    public function syncTheProduct(\WC_Product $local_product_obj, $remote_product_obj, $local_sku_str, $logger)
    {
        $product_on_sale = false;
        $sale_price = $remote_product_obj->PriceWithDiscount;
        $sale_from_date = $remote_product_obj->StartDate;
        $sale_to_date = $remote_product_obj->EndDate;

        $discount_percentage = $remote_product_obj->DiscountPercentage;

        if (!empty($sale_from_date) && !empty($sale_to_date) && !empty($sale_price)) {
            $local_product_obj->set_date_on_sale_from( $sale_from_date);
            $local_product_obj->set_date_on_sale_to( $sale_to_date);
            $local_product_obj->set_sale_price($sale_price);

            $logger->info('['.$local_sku_str.'] - ['.$local_product_obj->get_name().'] - Sale Added: ['.$sale_price.']['.$discount_percentage.']');
            $logger->info('['.$local_sku_str.'] - ['.$local_product_obj->get_name().'] - From ['.$sale_from_date.'] To ['.$sale_to_date.']');
            $local_product_obj->save();

            $product_on_sale = true;

        } else {
            $logger->info('['.$local_sku_str.'] - ['.$local_product_obj->get_name().'] - No Sale Active on Product');
        }

        return [
            'product_on_sale' => $product_on_sale
        ];
    }
}