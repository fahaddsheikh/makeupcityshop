<?php
namespace SyProducts;

use SyProducts\Application\Apis\BaseApi;
use SyProducts\Application\Pages\SyncPage;
use SyProducts\Application\Schedules\Hourly;

class App {
	
	public function __construct() 
	{

		register_activation_hook( SYNC_PRODUCT_PLUGIN_FILE, array($this,'pluginActivationHook' ));

		register_activation_hook( SYNC_PRODUCT_PLUGIN_FILE, array($this,'pluginDeactivationHook' ));

		add_action( 'init', array($this, 'iniCallback') );

		add_action('admin_menu', array($this,'adminPagesRegister') );


	}

	public function pluginActivationHook () 
	{
        $this->createDirInUploads();
	}

	public function pluginDeactivationHook ()
	{
		wp_clear_scheduled_hook( 'sync_products' );
	}

	public function iniCallback() 
	{
		if ( !wp_next_scheduled ( 'sync_products' ) ) {

            wp_schedule_event(time(), 'hourly', 'sync_products');

        }
		add_action('sync_products', array($this,'syncProducts'));

        if ( !wp_next_scheduled ( 'send_sync_logs_attachment' ) ) {

            wp_schedule_event(time(), 'daily', 'send_sync_logs_attachment');

        }
        add_action('send_sync_logs_attachment', array($this,'sendSyncLogsAttachment'));

	}

    public function sendSyncLogsAttachment()
    {
//        $attachments = BaseApi::getLogsDirPath().'/'.date('Y-m-d').'.txt';
        $log_file_urls='';
        $directory = BaseApi::getLogsDirPath();
        $uploads_dir = wp_upload_dir();
        $bae_upload_url = $uploads_dir['url'].'sync-products/logs/'.date("Y-m-d").'/';
        if (is_dir($directory)){
            if ($opendirectory = opendir($directory)){
                while (($file = readdir($opendirectory)) !== false){
                    $log_file_urls .=$bae_upload_url.$file.PHP_EOL;
                }
                closedir($opendirectory);
            }
        }


        wp_mail(esc_attr(get_option('sync_emails')), get_bloginfo('name') , 'Following log files generated Today:'.$log_file_urls);
	}
	
	
	public function syncProducts ()
	{
        $hourlySchedule = new Hourly();
        $hourlySchedule->syncHourlyData();
	}

	public function adminPagesRegister ()
	{
		$syncPage = new SyncPage();
	}

    public function createDirInUploads()
    {
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $upload_dir = $upload_dir . '/sync-products';
        if (! is_dir($upload_dir)) {
            mkdir( $upload_dir, 0777 );
        }
    }
}