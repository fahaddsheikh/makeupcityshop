<?php
/*
Plugin Name: Woo Custom APIs
Description: add custom api end points for woocommerce.
Author: Shah RUkh
Version: 1.0
*/

add_action( 'rest_api_init', function () {
    register_rest_route( 'wc/v3', '/sync-order/(?P<id>\d+)', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'update_order_meta',
        'args' => array(
            'id' => array(
                'validate_callback' => function($param, $request, $key) {
                    return is_numeric( $param );
                }
            ),
        ),
    ) );
} );

function update_order_meta(WP_REST_Request $request) {
     $order_id = $request['id'];
     if(wc_get_order($order_id)) {
         update_post_meta($request['id'], "_is_synced_with_technosys","yes");
         $response_arr = array(
             'success'	=> true,
             'message'	=> "Order Sync Status Updated",
         );
         return new WP_REST_Response( $response_arr, 200 );
     } else {
         $response_arr = array(
             'success'	=> false,
             'message'	=> "No Order found with the ID",
         );
         return new WP_REST_Response( $response_arr, 401 );
     }
}
