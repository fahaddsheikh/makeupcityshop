<?php
/**
 * Plugin Name: Woocommerce Trax Logistics
 * Description: woocommerce send sms notification to customer
 * Version: 1.0
 * Author: Onx Digital
 * Text Domain: onx-sms
 * License: GPL2
 *
*/

/**
 * Handle Trax_Citiess Class
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Trax_Cities
{
	
	private $api_key;
	private $api_password;

	function __construct()
	{
		$this->api_url = 'https://app.sonic.pk/api/cities';
		$this->api_key = 'YzlJbkRCZjRVaWk0TFBqQWlRZ3k5OFBzYjFmTjZ3Y2lBSGtRNnB3Wk9jWHo1Q0tYcTN3Z1hJZGIzbGdu5c9c99356586a';

		add_action( 'init', array($this, 'iniCallback') );


		add_action( 'add_meta_boxes', array($this, 'trax_cities') );
		add_filter('woocommerce_checkout_fields', array($this, 'change_city_to_dropdown'));
		add_filter( 'woocommerce_admin_billing_fields' , array($this, 'admin_city_select_field') );
		add_filter( 'woocommerce_admin_shipping_fields' , array($this, 'admin_city_select_field') );
	}

	public function iniCallback() 
	{
		if ( !wp_next_scheduled ( 'sync_trax_cities' ) ) {

            wp_schedule_event(time(), 'weekly', 'sync_trax_cities');

        }
		add_action('sync_trax_cities', array($this,'sync_trax_cities'));

		$this->init_cities();

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function init_cities()
	{
		$cities = json_decode(get_option('trax_cities'), true);

		foreach ($cities as $city) {
			$this->cities[ltrim(rtrim($city))] = $city;
		}
	}

	/**
	 * Leopard city metabox callback
	 *
	 * @return void
	 * @author 
	 **/
	public function sync_trax_cities( )
	{
	    $curl_handle = curl_init();

	    curl_setopt_array($curl_handle, array(
	      CURLOPT_URL => $this->api_url,
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 0,
	      CURLOPT_FOLLOWLOCATION => true,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "GET",
	      CURLOPT_HTTPHEADER => array(
	        "Authorization: $this->api_key"
	      ),
	    ));

	    $buffer = curl_exec($curl_handle);
	    curl_close($curl_handle);

	    $city_array = json_decode( $buffer, true )['cities'];

	    foreach ($city_array as $city) {
	    	$cities[$city['id']] = $city['name'];
	    }

	    update_option( 'trax_cities', json_encode($cities) );
	}

	/**
	 * Add leopard city metaboxes
	 *
	 * @return void
	 * @author 
	 **/
	public function trax_cities() {
	    add_meta_box(
	        'trax_cities',
	        'Trax Cities',
	        array($this, 'trax_cities_callback'),
	        'shop_order',
	        'side',
	        'low'
	    );
	}

	/**
	 * Show Trax Cities in the metabox
	 *
	 * @return void
	 * @author 
	 **/
	public function trax_cities_callback()
	{
		$cities = json_decode(get_option('trax_cities'));

		if ($cities) {
			echo "<ul>";
			foreach ($cities as $city) {
				echo "<li>" . $city . "</li>";
			}
			echo "</ul>";
		} else {
			echo 'Trax cities not stored. Please contact the site Administrator';
		}
	}

	/**
	 * Change the checkout city field to a dropdown field.
	 */
	public function change_city_to_dropdown($fields)
	{
	    $city_args = wp_parse_args(array(
	        'type' => 'select',
	        'options' => $this->cities
	    ), $fields['shipping']['shipping_city']);
	    $fields['shipping']['shipping_city'] = $city_args;
	    $fields['billing']['billing_city'] = $city_args; // Also change for billing field
	    return $fields;
	}

	function admin_city_select_field( $fields ) {

	    $fields['city'] = array(
	        'label'   => __( 'City', 'woocommerce' ),
	        'show'    => false,
	        'class'   => 'js_field-city select short',
	        'type'    => 'select',
	        'options' => $this->cities
	    );

	    return $fields;
	}
}

new Trax_Cities();