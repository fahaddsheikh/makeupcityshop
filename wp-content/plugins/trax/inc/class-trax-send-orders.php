<?php
/**
 * Plugin Name: Woocommerce Trax Logistics
 * Description: woocommerce send sms notification to customer
 * Version: 1.0
 * Author: Onx Digital
 * Text Domain: onx-sms
 * License: GPL2
 *
*/

/**
 * Handle Trax_Send_Orderss Class
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Trax_Send_Orders
{
	
	private $api_key;
	private $api_password;

	function __construct()
	{
		$this->api_url = 'https://app.sonic.pk/api/shipment/book';
		$this->api_key = 'YzlJbkRCZjRVaWk0TFBqQWlRZ3k5OFBzYjFmTjZ3Y2lBSGtRNnB3Wk9jWHo1Q0tYcTN3Z1hJZGIzbGdu5c9c99356586a';

		add_action('woocommerce_order_status_dispatched', array($this, 'dispatched_order_send_to_courier'), 200, 2);

	}
	 
	/** 
	 * Send to courier when order status changed
	 * 
	**/
	public function dispatched_order_send_to_courier( $order_id, $order ) {
		
		$city = $this->get_order_city( $order->get_billing_city() );

		if ( !is_null($city) ) {
			$number_of_items = count($order->get_items());
			$total_amount = $order->get_total();
			$address = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2();
			$name = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
			$email = $order->get_billing_email();
			$phone = $order->get_billing_phone();
			$note = $order->get_customer_note();

			$place_order = $this->place_order_at_courier($number_of_items, $total_amount, $order_id, $city, $name, $email, $phone, $address, $note);

			var_dump($place_order);
			die();

			$place_order_array = json_decode( $place_order, true );
			$place_order_status = $place_order_array['status'];

			if ( function_exists( 'wc_st_add_tracking_number' ) && $place_order_status == 1 ) {

				$tracking_number = $place_order_array['track_number'];
				$slip_link = $place_order_array['slip_link'];

				wc_st_add_tracking_number( $order_id, $tracking_number, 'Makeup City Shop', strtotime($order->get_date_modified()), $slip_link );
				$order->update_meta_data( 'dispatch_date', $order->get_date_modified() );
				$order->save();

			} else {
				$order->update_status('processing');
				$logger = wc_get_logger();
				$logger->error(
					sprintf( 'Status transition of order #%d errored!', $order_id ), array(
						'order' => $order,
						'error' => $place_order_array['error'],
					)
				);

				$order->add_order_note( __( 'Error during status transition. Recieved the following error from leopard: ', 'woocommerce' ) . ' ' . $place_order_array['error'] );
				
			}
		} else {
			$order->update_status('processing');
			$logger = wc_get_logger();
			$logger->error(
				sprintf( 'Status transition of order #%d errored!', $order_id ), array(
					'order' => $order,
					'error' => $place_order_array['error'],
				)
			);

			$order->add_order_note( __( 'Error during status transition. Order City not found in Leopards. Please update the city from Leopard Cities below and dispatch again. City: ', 'woocommerce' ) . ' ' . $city );

		}

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function get_order_city($city_required)
	{
		$stored_cities = json_decode( get_option('trax_cities'), true );

		if ($stored_cities) {
			$city_required = urlencode( strtolower( preg_replace('/\s+/', '_', $city_required) ) );

			echo "<pre>";
			var_dump($city_required);
			echo "</pre>";

			foreach ($stored_cities as $city_key => $city) {
				$cities[$city_key] = urlencode( strtolower( preg_replace('/\s+/', '_', $city) ) );
			}

			$city_key = array_search($city_required, $cities, TRUE);		

			return absint($city_key);
		}

		return null;
	}

	/**
	 * Place order at courier
	 *
	 * @return void
	 * @author 
	 **/
	public function place_order_at_courier($number_of_items, $total_amount, $order_id, $destination_city, $customer_name, $customer_email, $customer_phone, $customer_address, $customer_note)
	{
		$customer_note = empty($customer_note) ? 'None' : $customer_note;
		
		$order_array = array(
			'service_type_id'				=> 1,
		    'pickup_address_id' 			=> 2271,
		    'information_display'          	=> 1,
		    'consignee_city_id'             => $destination_city,
		    'consignee_name'         		=> $customer_name,
		    'consignee_address'           	=> $customer_address,
		    'consignee_phone_number_1'      => $customer_phone,
		    'consignee_email_address'       => $customer_email,
		    'order_id'        				=> $order_id,    
		    'item_product_type_id'			=> 8,
		    'item_description'				=> 'Cosmetic Products',
		    'item_quantity'					=> absint( $number_of_items ),
		    'item_insurance'				=> 0,
		    'pickup_date'					=> '2020-11-30',
		    'special_instructions'          => $customer_note, 
		    'estimated_weight'				=> '200',
		    'shipping_mode_id'				=> 1,
		    'amount'  						=> absint($total_amount),
		    'payment_mode_id'				=> 1,
		    'charges_mode_id'				=> 4
		);

		$curl_handle = curl_init();

		curl_setopt_array($curl_handle, array(
			CURLOPT_URL => $this->api_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_POSTFIELDS => $order_array,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_HTTPHEADER => array(
				"Authorization: $this->api_key"
			)
		));

		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		return $buffer;
	}

	/**
	 * Add leopard city metaboxes
	 *
	 * @return void
	 * @author 
	 **/
	public function dispatch_date() {
	    add_meta_box(
	        'dispatch_date',
	        'Dispatch Date',
	        array($this, 'dispatch_date_callback'),
	        'shop_order',
	        'side',
	        'low'
	    );
	}

	/**
	 * Leopard city metabox callback
	 *
	 * @return void
	 * @author 
	 **/
	public function dispatch_date_callback( $post )
	{
	    $dispatch_date = get_post_meta( $post->ID, 'dispatch_date', true );
	    if (!empty($dispatch_date)) {
	    	echo date('d M Y', strtotime($dispatch_date));
	    } else {
	    	echo "No dispatch date assigned";
	    }
	    
	    
	}
	
}

new Trax_Send_Orders();