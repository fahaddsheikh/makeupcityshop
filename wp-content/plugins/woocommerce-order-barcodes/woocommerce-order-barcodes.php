<?php
/**
 * Plugin Name: Woocommerce Order Barcodes
 * Description: woocommerce generate barcode from order number and print it on the invoice.
 * Version: 1.0
 * Author: Onx Digital
 * Text Domain: onx-barcodes
 * License: GPL2
 *
*/

if ( !defined( 'ABSPATH' ) ) exit;
require 'vendor/autoload.php';

register_activation_hook( __FILE__, array( 'WC_Order_Barcodes', 'onx_barcode_plugin_activate' ) );
register_uninstall_hook( __FILE__, array( 'WC_Order_Barcodes', 'onx_barcode_plugin_uninstall' ) );

if ( !class_exists( 'WC_Order_Barcodes' ) ) {
	class WC_Order_Barcodes {
		
		public function __construct()
		{
			if (in_array('woocommerce-pip/woocommerce-pip.php', apply_filters('active_plugins', get_option('active_plugins')))) {
				add_shortcode('order-barcode', array($this, 'barcodeByOrderId') );
				add_filter( 'wc_pip_document_company_address', array($this, 'barcode_company_address'), 10, 3);
				add_filter( 'wc_pip_get_settings_sections', array($this,'barcode_add_section'));
				add_filter( 'wc_pip_settings', array($this, 'get_barcode_settings'), 10, 2 );
				add_action( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'onx_barcode_plugin_action_links' ) );
			} else {
				add_action('admin_notices', array($this, 'onx_barcode_admin_notice'));
			}
		}
		
		public function onx_barcode_plugin_action_links($links)
		{
			$links = array_merge( 
				array('<a href="' . esc_url( admin_url( '/admin.php?page=wc-settings&tab=pip&section=barcode' ) ) . '">' . __( 'Settings' ) . '</a>'), $links 
			);
			return $links;	
		}
		
		public function barcode_add_section($sections)
		{
			$sections['barcode'] = __( 'Barcode', 'onx-barcode' );
			return $sections; 
		}
		
		public function get_barcode_settings( $settings, $current_section ) {
			if($current_section=='barcode') {
				$settings_barcode = array(
					'section_title' => array('name' => esc_html( 'Barcode Settings', 'onx-barcode'), 'type' => 'title', 'id' => 'wc_settings_barcode_section_title'),
						array('name' => esc_html( 'Show on invoice', 'onx-barcode' ), 'type' => 'checkbox', 'id'  => 'wc_settings_barcode_show' ),
						array('name' => esc_html( 'Width', 'onx-barcode' ), 'type' => 'text', 'id'  => 'wc_settings_barcode_width' ),
						array('name' => esc_html( 'Height', 'onx-barcode' ), 'type' => 'text', 'id' => 'wc_settings_barcode_height' ),	
						array('name' => esc_html( 'Color', 'onx-barcode'), 'type' => 'text', 'id' => 'wc_settings_barcode_color' ),		
					'section_end' => array('type' => 'sectionend', 'id' => 'wc_settings_barcode_section_end')
				);
				return $settings_barcode;
			} else {
				return $settings;
			}
		}
		
		
		public function barcode_company_address($company_address, $order_id, $type){
			$barcode_show = get_option('wc_settings_barcode_show');
			$barcode	= $company_address;
			if($barcode_show=='yes') $barcode = $barcode.'<p>'.do_shortcode('[order-barcode order_id='.$order_id.']').'</p>';
			return $barcode;
		}
		
		public function barcodeByOrderId($atts)
		{
			// This will output the barcode as HTML output to display in the browser
			$order_id = $atts['order_id'];
			$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
			$barcode_width = get_option('wc_settings_barcode_width');		
			$barcode_height = get_option('wc_settings_barcode_height');
			$barcode_color = get_option('wc_settings_barcode_color');
			return $generator->getBarcode($order_id, $generator::TYPE_CODE_128, $barcode_width, $barcode_height, $barcode_color);
		}
		
		public function onx_barcode_admin_notice()
		{
			echo '<div class="notice notice-warning is-dismissible"><p>'.esc_html('Install WooCommerce Print Invoices/Packing Lists to Use Barcode','onx-barcode').'</p></div>';
		}
		
		public function onx_barcode_plugin_activate()
		{
			add_option('wc_settings_barcode_show','yes');
			add_option('wc_settings_barcode_width', 2);
			add_option('wc_settings_barcode_height', 30);
			add_option('wc_settings_barcode_color', '#000');		
		}
		
		public function onx_barcode_plugin_uninstall()
		{
			delete_option('wc_settings_barcode_show');
			delete_option('wc_settings_barcode_width');
			delete_option('wc_settings_barcode_height');
			delete_option('wc_settings_barcode_color');
		}
	}
	$barcodeObj	= new WC_Order_Barcodes;
}