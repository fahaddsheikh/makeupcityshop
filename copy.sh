#!/bin/bash
cp -rf /tmp/makeupcity/* /var/www/vhosts/makeupcityshop.com/
cp -rf /tmp/makeupcity/.htaccess /var/www/vhosts/makeupcityshop.com/
cd /var/www/vhosts/makeupcityshop.com/
aws s3 cp s3://makeupcity-secrets/conf/wp-config.php .
chown -R www-data.www-data /var/www/vhosts/makeupcityshop.com/
# Trigger Pipeline