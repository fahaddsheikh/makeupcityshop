<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Order Status</title>
</head>

<body>
<?php 
include 'wp-load.php';		

function sendSMS($order_id, $textMessage,$status)
{
	$lifetimesms_api_token 	= 	get_option('wc_settings_onxsms_lifetimesms_api_token');
	$lifetimesms_api_secret = 	get_option('wc_settings_onxsms_lifetimesms_api_secret');
	$lifetimesms_send_from 	= 	get_option('wc_settings_onxsms_lifetimesms_send_from');		
	$order  				= 	wc_get_order($order_id);	
	$customer_mobile  		=  	get_post_meta( $order_id, '_billing_phone', true );				
	$url = "http://www.outreach.pk/api/sendsms.php/sendsms/url";
	$type	=	"xml";
	$lang	=	"English";				
	$parameters = "id=".$lifetimesms_api_token."&pass=".$lifetimesms_api_secret."&msg=".$textMessage."&to=".$customer_mobile."&lang=".$lang."&mask=".$lifetimesms_send_from.
	"&type=".$type;		
	$ch = curl_init();
	$timeout  =  30;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
	$response = curl_exec($ch);	
	$xmlRes = simplexml_load_string($response);
	curl_close($ch);
	return $xmlRes->code; 
}


global $wpdb;

$filter_date	= strtotime('15-NOV-2020');

$orders = wc_get_orders(
	array(
    	'limit'=>-1,
    	'type'=> 'shop_order',
    	'status'=> array( 'wc-on-hold'),
		'date_created'	=> '>' . ( $filter_date )
    )
);

foreach($orders as $order)
{
	$order_status  	= $order->get_status();
   	if($order_status=='on-hold') 
	{
		$order_id = $order->get_ID();
		$message = "Dear customer, we are overwhelmed with the response we have received from your end. Our team is working tirelessly to ensure timely delivery. But please note that it may take slightly longer than usual to get your orders delivered. Your patience is highly appreciated. For further queries please call at 03000770385";
		$response = sendSMS($order_id, $message, $order_status);
		if($response==300) $order->add_order_note( __('Order '.$order_status.' SMS send to buyer successfully.', 'onxsms' ) );
	}					
}
echo '<p><b>SMS sent to '.count($orders).' Customer</b></p>';
?>
</body>
</html>